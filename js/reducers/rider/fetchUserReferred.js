import { FETCH_USERS_REFERRED } from '../../actions/rider/fetchReferredUser';

const initialState = {
    referredUsers: []
};

const fetchedUsers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERS_REFERRED:
            return {
                ...state,
                referredUsers: action.payload
            }
    }
}