import React, { Component } from 'react';
import { View, Text, Button, Modal, StyleSheet } from 'react-native';
import config from '../../config';
import renderIf from 'render-if';
import WebViewBraintree from 'react-native-webview-braintree/WebViewBraintree'
import axios from 'axios';

export default class componentName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clientToken: '',
            paymentAPIResponse: '',
            modalVisible: true,
            price: 0
        };
    }

    /*
    called by BraintreePaymentWebview once a nonce is recieved by
    the webview and posts the purchase to the applicationServer
   */
    handlePaymentMethod = nonce => {
        // make api call to purchase the item using the nonce received
        // from BraintreeWebView Component
        axios.post('http://192.168.1.15:3010/api/payment/paymentGateway', {
            nonce: nonce, amount: this.state.price
        })
            .then(response => {
                if (response.data.success === true) {
                    this.setState({ paymentAPIResponse: 'PAYMENT_SUCCESS' });
                } else {
                    this.setState({ paymentAPIResponse: 'PAYMENT_REJECTED' });
                }
            });
    };


    closeModal() {
        this.setState({ modalVisible: false, clientToken: '' })
    }
    onPayClick(priceToPay) {
        this.setState({ price: priceToPay })
        fetch(`${config.serverSideUrl}:${config.port}/api/payment/paymentGateway`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })
            .then(resp => resp.json())
            .then(response => {
                if (response.type === 'success') {
                    let token = response.clientToken;
                    this.setState({ clientToken: token });
                }
            })
    }

    render() {
        if (this.state.clientToken !== '') {
            return (
                <View style={styles.modalContainer}>
                    <Modal>
                        <View style={{ flex: 1, height: 100, backgroundColor: "red" }}>
                            <WebViewBraintree
                                clientToken={this.state.clientToken}
                                nonceObtainedCallback={this.handlePaymentMethod}
                                navigationBackCallback={this.navigationBackCallback}
                                paymentAPIResponse={this.state.paymentAPIResponse}
                            />
                        </View>
                        <Button
                            style={{ flex: 1, alignItems: 'flex-end' }}
                            onPress={() => this.closeModal()}
                            title="Close modal"
                        >
                        </Button>
                    </Modal>
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>

                    <Button
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}
                        onPress={() => this.onPayClick(100)}
                        title="click here to pay"
                        color="#841584"
                    />
                </View>
            )
        }
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'grey',
    },
    innerContainer: {
        alignItems: 'center',
    },
});