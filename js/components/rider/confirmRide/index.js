import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Image,
  View,
  TouchableOpacity,
  Platform,
  Dimensions, Modal,
  Alert, SafeAreaView,
  NetInfo,
  BackHandler
} from "react-native";
import PropTypes from "prop-types";
import {
  Header,
  Text,
  Button,
  Grid,
  Col,
  Row,
  Icon,
  Card,
  CardItem,
  Title,
  Item,
  Input,
  Left,
  Right,
  Body,
  Toast,
  ListItem
} from "native-base";
import _ from "lodash";
import FIcon from "react-native-vector-icons/FontAwesome";
import DatePicker from 'react-native-datepicker';
import Spinner from "../../loaders/Spinner";
import { Actions, ActionConst } from "react-native-router-flux";
import { openDrawer } from "../../../actions/drawer";
import * as tripViewSelector from "../../../reducers/rider/tripRequest";
import {
  changePageStatus,
  clearTripAndTripRequest,
  currentLocation
} from "../../../actions/rider/home";
import {
  fetchAddressFromCoordinatesAsync,
  fetchFareDetail,
  setTripRequest,
  clearFare
} from "../../../actions/rider/confirmRide";
import { requestTrip, requestScheduleTrip } from "../../../services/ridersocket";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import ModalView from "../../common/ModalView";
import { getBoundry } from "../../../utils/bound";

const { width } = Dimensions.get("window");
const deviceHeight = Dimensions.get("window").height;
const image = require("../../../../assets/images/paytm2.png");

function mapStateToProps(state) {
  return {
    region: {
      latitude: state.rider.tripRequest.srcLoc[0],
      longitude: state.rider.tripRequest.srcLoc[1]
    },
    tripRequest: state.rider.tripRequest,
    srcLoc: state.rider.tripRequest.srcLoc,
    pickUpAddress: state.rider.tripRequest.pickUpAddress,
    destAddress: state.rider.tripRequest.destAddress,
    rider: state.rider.user,
    destLoc: state.rider.tripRequest.destLoc,
    tripRequestStatus: state.rider.tripRequest.tripRequestStatus,
    tripViewSelector: tripViewSelector.tripView(state),
    paymentOption: state.rider.paymentOption,
    appConfig: state.basicAppConfig.config,
    carType: state.rider.carType
  };
}

class ConfirmRide extends Component {
  static propTypes = {
    region: PropTypes.object,
    tripRequest: PropTypes.object,
    pickUpAddress: PropTypes.string,
    rider: PropTypes.object,
    changePageStatus: PropTypes.func,
    clearTripAndTripRequest: PropTypes.func,
    setTripRequest: PropTypes.func,
    fetchAddressFromCoordinatesAsync: PropTypes.func,
    tripViewSelector: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      showFareEstimate: false,
      srcCity: "",
      destCity: "",
      modalVisible: false,
      datetime: null,
      minDate: new Date(),
      dateError: false
    };
  }

  async componentDidMount() {
    await this.props.fetchAddressFromCoordinatesAsync(
      this.props.region.latitude,
      this.props.region.longitude
    );
    // this.props.clearFare();
  }
  // componentDidMount() {
  //   BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  // }

  // componentWillUnmount() {
  //   BackHandler.removeEventListener("hardwareBackPress", () =>
  //     this.backAndroid()
  //   ); // Remove listener
  // }

  // backAndroid() {
  //   this.goBack(),
  //   Actions.pop()
  // }
  bookScheduleRide() {
    if (this.state.datetime == null) {
      this.setState({
        dateError: true
      })
    } else {
      this.setState({
        dateError: false
      })
      this.setModalVisible(!this.state.modalVisible)
      const tripRequest = this.props.tripRequest;
    tripRequest.scheduleOn = this.state.datetime;

    Actions.ScheduleConfirm();
    requestScheduleTrip({
      carType: this.props.carType,
      tripRequest,
      rider: this.props.rider
    });

    console.log(tripRequest);
    }
  }


  async handlePress() {
    this.props.clearTripAndTripRequest(); // To make sure any previous trip is not present.
    // srcCity = await getBoundry(
    //   this.props.srcLoc,
    //   this.props.appConfig.googleMapsApiKey
    // );
    // destCity = await getBoundry(
    //   this.props.destLoc,
    //   this.props.appConfig.googleMapsApiKey
    // );
    /*
    // logic to bound the App for same city 
    if (srcCity !== destCity) {
      Alert.alert("Warning", "We Don't have service available here");
    } 
    */
    this.props.setTripRequest("request");
    requestTrip({
      // callingSocket
      carType: this.props.carType,
      tripRequest: this.props.tripRequest,
      rider: this.props.rider
    });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  getCurrentDateTime() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var hr = today.getHours();
    var min = today.getMinutes();
    if (hr < 10) {
      hr = '0' + hr
    }
    if (min < 10) {
      min = '0' + min
    }
    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' + dd;
    var time = hr + ':' + min + ':' + '00' + '.000z';
    this.setState({ scheduleDate: today, scheduleTime: time })
    this.setModalVisible(true);
    this.props.estimateModalStatus("rideLater")
  }
  fetchTripEstimate() {
    if (this.props.tripRequest.destAddress) {
      this.props.fetchFareDetail(this.props.tripRequest);
      this.setState({ showFareEstimate: true });
    } else {
      Alert.alert("Warning", "Set Up Destination Location");
    }
  }
  fareEstimate() {
    return (
      <ModalView>
        {this.props.tripRequest.tripAmt ||
          this.props.tripRequest.tripDistance ? (
            <View
              style={{
                backgroundColor: "transparent",
                padding: 20,
                height: deviceHeight / 2
              }}
            >
              <Card
                style={{
                  width: width - 30,
                  height: null,
                  borderRadius: 4,
                  justifyContent: "space-between"
                }}
              >
                <CardItem
                  style={{
                    marginHorizontal: 10,
                    borderBottomWidth: 1,
                    borderBottomColor: "#EFEDED"
                  }}
                >
                  <Body
                    style={{
                      paddingVertical: 15,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        fontWeight: "bold",
                        color: "#727376"
                      }}
                    >
                      ESTIMATED FARE
                  </Text>
                  </Body>
                </CardItem>
                <CardItem>
                  <Grid style={{ paddingHorizontal: 15, marginTop: -15 }}>
                    <Col
                      style={{
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        active
                        name="pin"
                        style={{ color: "#4d4d4d", fontSize: 40 }}
                      />
                      <Text style={{ color: "#4d4d4d", fontSize: 18 }}>
                        Distance
                    </Text>
                      <Text style={{ color: "#4d4d4d", fontWeight: "bold" }}>
                        {this.props.tripRequest.tripDistance}
                      </Text>
                    </Col>
                    <Col
                      style={{
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="time"
                        style={{ color: "#4d4d4d", fontSize: 40 }}
                      />
                      <Text style={{ color: "#4d4d4d", fontSize: 18 }}>Time</Text>
                      <Text style={{ color: "#4d4d4d", fontWeight: "bold" }}>
                        {this.props.tripRequest.tripTime}
                      </Text>
                    </Col>
                    <Col
                      style={{
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Text
                        style={{
                          color: "#4d4d4d",
                          fontSize: 40,
                          fontWeight: "bold"
                        }}
                      >
                        {_.get(
                          this.props.appConfig,
                          "quickestPrice.currencySymbol",
                          "$"
                        )}
                      </Text>
                      <Text style={{ color: "#4d4d4d", fontSize: 18 }}>Fare</Text>
                      <Text style={{ color: "#4d4d4d", fontWeight: "bold" }}>
                        {this.props.appConfig.quickestPrice.currencySymbol}
                        {this.props.tripRequest.tripAmt}
                      </Text>
                    </Col>
                  </Grid>
                </CardItem>
                <Button
                  full
                  info
                  onPress={() => {
                    this.setState({ showFareEstimate: false });
                  }}
                  style={{
                    backgroundColor: "#FFB600",
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                  }}
                >
                  <Text style={{ color: "white", fontWeight: "700" }}>
                    GOT IT
                </Text>
                </Button>
              </Card>
            </View>
          ) : (
            <View
              style={{
                backgroundColor: "transparent",
                padding: 20,
                height: deviceHeight / 3
              }}
            >
              <Card
                style={{
                  width: width - 30,
                  height: null,
                  borderRadius: 4,
                  justifyContent: "space-between"
                }}
              >
                <CardItem
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 40
                  }}
                >
                  <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                    No Possible Route
                </Text>
                </CardItem>
                <Button
                  full
                  info
                  onPress={() => {
                    this.setState({ showFareEstimate: false });
                  }}
                  style={{
                    backgroundColor: "#FFB600",
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                  }}
                >
                  <Text style={{ color: "white", fontWeight: "700" }}>
                    GOT IT
                </Text>
                </Button>
              </Card>
            </View>
          )}
      </ModalView>
    );
  }
  goBack() {
    this.props.changePageStatus("home");
  }
  getDate(datetime) {
    this.setState({ datetime: datetime })
  }
  confirmRide() {

  }
  checkInternat() {
    // NetInfo.isConnected.fetch().then(isConnected => {
    //   isConnected ? this.netOnline() : 
    //   this.netOffline()
    //     // Toast.show({
    //     //   text:"Device Offline",
    //     //   buttonText: "Okay", })
    // });
    function handleFirstConnectivityChange(isConnected) {
      isConnected ? null :
        Toast.show({
          text: "Device Offline",
          duration: 5000,
          buttonText: "Okay"
        })
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        handleFirstConnectivityChange
      );
    }
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      handleFirstConnectivityChange
    );
  }
  render() {
    this.checkInternat()
    return (
      <View pointerEvents="box-none" style={{ flex: 1 }}>
        {/* =============================================================================================================== */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{
            flex: 1,
            backgroundColor: "transparent",
            justifyContent: "center",
            alignItems: "center"
          }}>
            <View style={{
              borderRadius: 10,
              maxWidth: 350,
              justifyContent: 'center',
              ...Platform.select({
                ios: {
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 3,
                    height: 3
                  },
                  shadowRadius: 3,
                  shadowOpacity: 0.5
                },
                android: {
                  elevation: 2
                }
              }),
              alignItems: "center",
              width: Dimensions.get("screen").width * (3.5 / 4),
              backgroundColor: "white"
            }}>
              <View style={{
                backgroundColor: "#FFB600",
                width: "100%",
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                justifyContent: "center",
                alignItems: "center",
                height: 30,
                marginBottom: 20
              }}>
                <Text style={{ textAlign: "center", backgroundColor: "transparent", color: 'white' }}>Choose Date {"&"} Time</Text>
              </View>
              <DatePicker
                style={{
                  width: "90%",
                  marginBottom: 20,
                }}
                date={this.state.datetime}
                mode="datetime"
                minDate={this.state.minDate}
                format="YYYY-MM-DD HH:mm"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                onDateChange={(datetime) => { this.getDate(datetime) }}
              />
              {this.state.dateError == false ? null : <Text style={{ color: "red", marginBottom: 15 }}>Please select Date {"&"} Time</Text>}
              <View style={{ flexDirection: "row", justifyContent: "space-around", width: "100%" }}>
                <TouchableOpacity
                  onPress={() => {
                    this.bookScheduleRide()
                    // this.state.datetime == this.state.minDate ?
                    //   [Alert.alert("Warning", "Please select date and time"), this.setModalVisible(!this.state.modalVisible)] :
                    //   this.bookScheduleRide()
                  }}>
                  <View style={styles.confirmBtn}>
                    <Text style={{ color: "white" }}>Confirm Ride</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible)
                  }}>
                  <View style={[styles.confirmBtn, { width: 90 }]}>
                    <Text style={{ color: "white" }}>Cancel</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        {/* =============================================================================================================== */}
        {this.state.showFareEstimate ? this.fareEstimate() : null}
        <View style={{ position: "absolute", top: 0, width: width + 5, backgroundColor: "transparent" }}>
          <SafeAreaView style={{ backgroundColor: "#FFB600" }}>
            <Header
              androidStatusBarColor="#FFB600"
              iosBarStyle="light-content"
              style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
            >
              {!this.props.tripViewSelector.backButton ? (
                <Left>
                  <TouchableOpacity onPress={() => this.goBack()}>
                    <Button transparent onPress={() => this.goBack()}>
                      <Icon
                        color="white"
                        name="md-arrow-back"
                        style={{ fontSize: 28, color: "#fff" }}
                      />
                    </Button>
                  </TouchableOpacity>
                </Left>
              ) : (
                  <Left />
                )}
              <Body>
                <Title style={{ color: "#fff" }}>
                  Confirmation
              </Title>
              </Body>
              <Right />
            </Header>
          </SafeAreaView>
          <View
            style={Platform.OS === "ios" ? styles.iosSrcdes : styles.aSrcdes}
          >
            <View style={styles.searchBar}>
              <View>
                <Item
                  regular
                  style={{
                    backgroundColor: "#FFF",
                    borderWidth: 0,
                    marginLeft: 0,
                    borderColor: "transparent"
                  }}
                >
                  <Icon name="ios-search" style={styles.searchIcon} />
                  <Button
                    onPress={() => {
                      Actions.suggestLocation({
                        heading: "Starting Location",
                        page: "home"
                      });
                    }}
                    transparent
                    style={{ flex: 1, paddingLeft: 10, fontSize: 16 }}
                  >
                    <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                      {_.get(this.props, "pickUpAddress", "Source Required")}
                    </Text>
                  </Button>
                </Item>
              </View>
            </View>
            <View style={styles.searchBar}>
              <View>
                <Item
                  regular
                  style={{
                    backgroundColor: "#FFF",
                    marginLeft: 0,
                    borderColor: "transparent"
                  }}
                >
                  <Icon name="ios-search" style={styles.searchIcon} />
                  <Button
                    onPress={() => {
                      Actions.suggestLocation({
                        heading: "Destination Location",
                        page: "destination"
                      });
                    }}
                    transparent
                    style={{ flex: 1, paddingLeft: 10, fontSize: 16 }}
                  >
                    {this.props.destAddress !== "" ? (
                      <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                        {_.get(
                          this.props,
                          "destAddress",
                          "Enter Drop Location"
                        )}
                      </Text>
                    ) : (
                        <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                          Enter Drop Location
                      </Text>
                      )}
                  </Button>
                </Item>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.slideSelector}>
          <Card style={{ flexDirection: "row", bottom: -6, left: -3 }}>
            <CardItem
              style={styles.selectCardContainer}
              button
              onPress={() => this.fetchTripEstimate()}
            >
              <View style={{ ...styles.selectCard, flexDirection: "row" }}>
                <Icon
                  name="md-alarm"
                  style={{
                    top: 5,
                    color: "#7FBFE2",
                    width: null,
                    fontSize: 20,
                    marginRight: 5
                  }}
                />
                <Text
                  style={{
                    fontSize: 14,
                    color: "#2E6894",
                    fontWeight: "600",
                    height: 25,
                    lineHeight: 26,
                    top: Platform.OS === "ios" ? 5 : 0
                  }}
                >
                  TRIP ESTIMATE
                </Text>
              </View>
            </CardItem>
            <CardItem
              style={styles.selectCardContainer}
              button
              onPress={() => Actions.cardPayment()}
            >
              <View style={{ ...styles.selectCard, flexDirection: "row" }}>
                {this.props.paymentOption.ridePayment ? (
                  <Icon
                    name={
                      this.props.paymentOption.paymentMethod === "CARD"
                        ? "ios-card"
                        : "ios-cash"
                    }
                    style={styles.iosIcon}
                  />
                ) : (
                    <Icon
                      name="ios-alert-outline"
                      style={{
                        top: 6,
                        color: "red",
                        width: null,
                        fontSize: 20,
                        marginRight: 5
                      }}
                    />
                  )}
                <Text
                  style={{
                    fontSize: 14,
                    height: 25,
                    fontWeight: "600",
                    lineHeight: 26,
                    top: Platform.OS === "ios" ? 5 : 0
                  }}
                >
                  {this.props.paymentOption.paymentMethod}
                </Text>
              </View>
            </CardItem>
          </Card>

          <View style={{ flexDirection: "row", paddingTop: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "transparent" }}>
            <Button
              full
              onPress={() =>
                this.props.destAddress == "" || undefined ?
                  Alert.alert("Warning", "Set Up Destination Location") :
                  this.props.paymentOption.ridePayment == "" || null || undefined ?
                    Alert.alert("Warning", "Select payment option") :
                    this.props.tripViewSelector.loadSpinner ? alert('Please Wait, Your Ride request is in Progress') :
                      this.handlePress("rideBooked")}
              // && this.props.paymentOption.ridePayment == " " || null || undefined
              // disabled={
              //   !(
              //     // this.props.paymentOption.ridePayment &&
              //     // this.props.pickUpAddress &&
              //     this.props.destAddress

              //   )
              // }
              style={{
                flex: 3,
                backgroundColor: "#fff",
                borderRadius: 5,
                height: 50,
                marginBottom: 30,
                marginRight: 10
                // width: Dimensions.get('screen').width * (1/5),
              }}
            >
              {this.props.tripViewSelector.loadSpinner ? (
                <Spinner />
              ) : (
                  <Text style={{ color: "#000", fontWeight: "500", fontSize: 20 }}>
                    Request Taxi
              </Text>
                )}
            </Button>
            <Button
              full
              style={{
                flex: 1,
                backgroundColor: "#fff",
                borderRadius: 5,
                height: 50,
                marginBottom: 30,
                // width: Dimensions.get('screen').width * (1/5),
              }}
              onPress={() => {
                this.props.destAddress == "" || undefined ?
                  Alert.alert("Warning", "Set Up Destination Location") :
                  this.props.paymentOption.ridePayment == "" || null || undefined ?
                    Alert.alert("Warning", "Select payment option") :
                    this.props.tripViewSelector.loadSpinner ? alert('Please Wait, Your Ride request is in Progress') :
                      this.setModalVisible(true);
              }}
            >
              {this.props.tripViewSelector.loadSpinner ? (
                <Spinner />
              ) : (
                  <Image source={require("../../../../assets/images/scheduleIcon.png")}
                    style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                  // <Text style={{ color: "#fff", fontSize: 18, fontWeight: "500" }}>
                  //   {I18n.t('Ride Later')}
                  // </Text>
                )}
            </Button>
          </View>
        </View>
      </View >
    );
  }
}

function bindActions(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    fetchAddressFromCoordinatesAsync: (latitude, longitude) =>
      dispatch(fetchAddressFromCoordinatesAsync(latitude, longitude)),
    // noNearByDriver: () => dispatch(noNearByDriver()),
    currentLocation: () => dispatch(currentLocation()),
    setTripRequest: status => dispatch(setTripRequest(status)),
    clearTripAndTripRequest: () => dispatch(clearTripAndTripRequest()),
    fetchFareDetail: tripCoordinates =>
      dispatch(fetchFareDetail(tripCoordinates)),
    changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    clearFare: () => dispatch(clearFare())
  };
}

export default connect(mapStateToProps, bindActions)(ConfirmRide);

