import * as Expo from "expo";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Platform,
  Dimensions,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  TouchableHighlight,
  ActivityIndicator, Modal,
  Image, SafeAreaView,
  NetInfo, ScrollView, Picker,
} from "react-native";
import _ from "lodash";
import EIcon from 'react-native-vector-icons/Entypo';
import PropTypes from "prop-types";
import Model from "react-native-modalbox";
import {
  Header,
  Text,
  Button,
  Icon,
  Card,
  Title,
  Item,
  Input,
  List,
  ListItem,
  Left,
  Right,
  Body,
  CardItem,
  Grid,
  Col,
  Row,
  Toast
} from "native-base";
import { Actions, ActionConst } from "react-native-router-flux";
import config from '../../../../config';
import { openDrawer } from "../../../actions/drawer";
import { changeCarType } from "../../../actions/rider/carTypeSelection";
import * as viewSelector from "../../../reducers/viewStore";
import {
  setSourceLocation,
  fetchPrediction,
  changeRegion,
  changePageStatus,
  fetchUserCurrentLocationAsync,
  fetchAddressFromCoordinatesAsync,
  currentLocation
} from "../../../actions/rider/home";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import { lang } from '../../data.json';
import DetectNavbar from 'react-native-detect-navbar-android';

const { width, height } = Dimensions.get("window");
const aspectRatio = width / height;

function mapStateToProps(state) {
  return {
    region: {
      latitude: state.rider.user.gpsLoc[0],
      longitude: state.rider.user.gpsLoc[1],
      latitudeDelta: state.rider.user.latitudeDelta,
      longitudeDelta: state.rider.user.latitudeDelta * aspectRatio
    },
    tripAmount: state.basicAppConfig,
    appLanguage: state.rider.appState.language,
    pickUpAddress: state.rider.tripRequest.pickUpAddress,
    currentAddress: state.rider.user.address,
    mapRegion: state.rider.tripRequest.srcLoc,
    jwtAccessToken: state.rider.appState.jwtAccessToken,
    email: state.rider.user.email,
    appConfig: state.basicAppConfig.config
  };
}

class Home extends Component {
  static propTypes = {
    changePageStatus: PropTypes.func,
    openDrawer: PropTypes.func,
    currentLocation: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      string: "",
      currentLatitude: "",
      currentLongitude: "",
      Economy: true,
      Premium: false,
      SUV: false,
      Luxury: false,
      passengers: 1,
      QuickestFare: "100%",
      SedanFare: "0%",
      StationWagonFare: "0%",
      LuxuryFare: "0%",
      WheelChairFare: "0%",
      MaxiFare: "0%",
      string: "",
      carType: "Quickest",
      modalVisible: false,
      modalPassVisible: false,
      currentLatitude: "",
      currentLongitude: "",
      baseFare: this.props.tripAmount.config.quickestPrice.baseFare,
      PerKM: this.props.tripAmount.config.quickestPrice.farePerKm,
      time: this.props.tripAmount.config.quickestPrice.farePerMin,
      showToast: false,
      netCheck: true,
      showWheelchairPrice: "WheelChairOne",
      showMaxiPrice: "MaxiFive"
    };
  }
  componentWillMount() {
    this.props.currentLocation();
  }
  setLocationClicked() {
    this.props.changePageStatus("confirmRide");
  }
  fetchMobileVerifed() {
    this.setState({ isLoading: true })
    fetch(`${config.serverSideUrl}:${config.port}/api/verify/mobile`, {
      method: 'GET',
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        // Authorization: jwtAccessToken,
        email: this.props.email
      }
    }).then(res => res.json())
      .then((result) => {

        if (result.verifiedStatus == true) {

          this.setLocationClicked();
          this.setState({ isLoading: false })
        } else {

          alert("Please, Verify Your Mobile Number first")
          Actions.OtpVerification();
          this.setState({ isLoading: false })
        }
      })

  }

  CarQuickest() {
    this.setState({
      QuickestFare: "100%",
      SedanFare: "0%",
      StationWagonFare: "0%",
      LuxuryFare: "0%",
      WheelChairFare: "0%",
      MaxiFare: "0%",
      baseFare: this.props.tripAmount.config.quickestPrice.baseFare,
      PerKM: this.props.tripAmount.config.quickestPrice.farePerKm,
      time: this.props.tripAmount.config.quickestPrice.farePerMin,
    });
  }
  CarSedan() {
    this.setState({
      QuickestFare: "0%",
      SedanFare: "100%",
      StationWagonFare: "0%",
      LuxuryFare: "0%",
      WheelChairFare: "0%",
      MaxiFare: "0%",
      baseFare: this.props.tripAmount.config.sedanPrice.baseFare,
      PerKM: this.props.tripAmount.config.sedanPrice.farePerKm,
      time: this.props.tripAmount.config.sedanPrice.farePerMin,
    });
  }
  CarStationWagon() {
    this.setState({
      QuickestFare: "0%",
      SedanFare: "0%",
      StationWagonFare: "100%",
      LuxuryFare: "0%",
      WheelChairFare: "0%",
      MaxiFare: "0%",
      baseFare: this.props.tripAmount.config.wagonPrice.baseFare,
      PerKM: this.props.tripAmount.config.wagonPrice.farePerKm,
      time: this.props.tripAmount.config.wagonPrice.farePerMin,
    });
  }
  CarLuxury() {
    this.setState({
      QuickestFare: "0%",
      SedanFare: "0%",
      StationWagonFare: "0%",
      LuxuryFare: "100%",
      WheelChairFare: "0%",
      MaxiFare: "0%",
      baseFare: this.props.tripAmount.config.luxuryPrice.baseFare,
      PerKM: this.props.tripAmount.config.luxuryPrice.farePerKm,
      time: this.props.tripAmount.config.luxuryPrice.farePerMin,

    });
  }
  CarWheelChair() {
    this.setState({
      QuickestFare: "0%",
      SedanFare: "0%",
      StationWagonFare: "0%",
      LuxuryFare: "0%",
      WheelChairFare: "100%",
      MaxiFare: "0%",
      baseFare: this.props.tripAmount.config.wheelchairOnePrice.baseFare,
      PerKM: this.props.tripAmount.config.wheelchairOnePrice.farePerKm,
      time: this.props.tripAmount.config.wheelchairOnePrice.farePerMin,
    });
  }
  CarMaxi() {
    this.setState({
      QuickestFare: "0%",
      SedanFare: "0%",
      StationWagonFare: "0%",
      LuxuryFare: "0%",
      WheelChairFare: "0%",
      MaxiFare: "100%",
      baseFare: this.props.tripAmount.config.maxiFivePrice.baseFare,
      PerKM: this.props.tripAmount.config.maxiFivePrice.farePerKm,
      time: this.props.tripAmount.config.maxiFivePrice.farePerMin,
    });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  focusSearch = () => {
    this.refs.searchInput._root.focus();
  };
  componentDidMount() {
    this.props.fetchAddressFromCoordinatesAsync(this.props.region);
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid());
  }
  async componentWillMount() {

    const status = await Expo.Permissions.getAsync(
      Expo.Permissions.REMOTE_NOTIFICATIONS
    );
    this.props.currentLocation();
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }
  backAndroid() { // Return to previous screen
    return true; // Needed so BackHandler knows that you are overriding the default action and that it should not close the app
  }
  netOffline() {
    if (this.state.netCheck == true) {
      if (this.state.netCheck == true) {
        null
      } if (this.state.netCheck == false) {
        alert("offline"),
          this.setState({
            netCheck: false
          })
      }
    }
  }
  netOnline() {
    if (this.state.netCheck == true) {
      null
    }
    if (this.state.netCheck == false) {
      this.setState({
        netCheck: true
      })
    }
  }
  checkInternat() {
    function handleFirstConnectivityChange(isConnected) {
      isConnected ? null :
        Toast.show({
          text: "Device Offline",
          duration: 5000,
          buttonText: "Okay"
        })
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        handleFirstConnectivityChange
      );
    }
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      handleFirstConnectivityChange
    );
  }
  setModalPassVisible(visible) {
    this.setState({ modalPassVisible: visible });
  }

  changeFareTypeMaxi(itemValue) {
    if (itemValue == "MaxiFIVE") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiFivePrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiFivePrice.farePerKm,
        time: this.props.tripAmount.config.maxiFivePrice.farePerMin
      });
    } else if (itemValue == "MaxiSIX") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiSixPrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiSixPrice.farePerKm,
        time: this.props.tripAmount.config.maxiSixPrice.farePerMin
      });
    } else if (itemValue == "MaxiSEVEN") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiSevenPrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiSevenPrice.farePerKm,
        time: this.props.tripAmount.config.maxiSevenPrice.farePerMin
      });
    }
    else if (itemValue == "MaxiEIGHT") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiEightPrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiEightPrice.farePerKm,
        time: this.props.tripAmount.config.maxiEightPrice.farePerMin
      });
    }
    else if (itemValue == "MaxiNINE") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiNinePrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiNinePrice.farePerKm,
        time: this.props.tripAmount.config.maxiNinePrice.farePerMin
      });
    }
    else if (itemValue == "MaxiTEN") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiTenPrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiTenPrice.farePerKm,
        time: this.props.tripAmount.config.maxiTenPrice.farePerMin
      });
    }
    else if (itemValue == "MaxiELEVEN") {
      this.setState({
        showMaxiPrice: itemValue,
        baseFare: this.props.tripAmount.config.maxiElevenPrice.baseFare,
        PerKM: this.props.tripAmount.config.maxiElevenPrice.farePerKm,
        time: this.props.tripAmount.config.maxiElevenPrice.farePerMin
      });
    }
  }


  render() {
    this.checkInternat()
    return (
      <View pointerEvents="box-none" style={{ flex: 1 }}>
        {/* ============================================================= */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalPassVisible}
          onRequestClose={() => { console.log('Modal has been closed.'); }}>
          <View style={{ flex: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
            <View style={{
              height: this.state.carType == "WheelChair" ? 200 : Platform.OS === 'ios' ? 200 : 150,
              width: 200,
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 5,
              ...Platform.select({
                ios: {
                  shadowColor: 'black',
                  shadowOffset: { height: 0, width: 0 },
                  shadowOpacity: 0.5,
                  shadowRadius: 5
                },
                android: {
                  elevation: 2
                }
              })
            }}>
              {this.state.carType == "WheelChair" ? <View>
                <View style={{ width: "100%", height: "100%", justifyContent: "space-between", alignItems: "center" }}>
                  <View style={{ backgroundColor: "#FFB600", width: 200, height: 30, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ color: "white" }}>Select No of Wheel Chairs</Text></View>
                  <Text onPress={() => { this.props.changeCarType("WheelChairOne"); this.setModalPassVisible(!this.state.modalPassVisible) }}>One WheelChair</Text>
                  <Text onPress={() => { this.props.changeCarType("WheelChairTwo"); this.setModalPassVisible(!this.state.modalPassVisible) }}>Two WheelChair</Text>
                  <TouchableOpacity
                    style={{ backgroundColor: "#FFB600", width: 200, height: 30, justifyContent: "center", alignItems: "center" }}
                    onPress={() => this.setModalPassVisible(!this.state.modalPassVisible)}>
                    <Text style={{ color: "white" }}>Close</Text>
                  </TouchableOpacity>
                </View>
              </View> : null}
              {this.state.carType == "Maxi" ? <View>
                <View style={{ width: "100%", height: "100%", justifyContent: "space-between", alignItems: "center" }}>
                  <View style={{ backgroundColor: "#FFB600", width: 200, height: 30, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ color: "white" }}>Choose No of Passengers</Text>
                  </View>

                  <Picker
                    selectedValue={this.state.passengers}
                    style={{ height: Platform.OS === 'ios' ? 170 : 50, width: 150 }}
                    onValueChange={
                      (itemValue, itemIndex) => {
                        this.setState({ passengers: itemValue });
                        this.props.changeCarType(itemValue);
                      }}>
                    <Picker.Item label="FIVE" value="MaxiFIVE" />
                    <Picker.Item label="SIX" value="MaxiSIX" />
                    <Picker.Item label="SEVEN" value="MaxiSEVEN" />
                    <Picker.Item label="EIGHT" value="MaxiEIGHT" />
                    <Picker.Item label="NINE" value="MaxiNINE" />
                    <Picker.Item label="TEN" value="MaxiTEN" />
                    <Picker.Item label="ELEVEN" value="MaxiELEVEN" />
                  </Picker>

                  <TouchableOpacity
                    style={{
                      backgroundColor: "#FFB600",
                      width: 200,
                      height: 30,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    onPress={() => this.setModalPassVisible(!this.state.modalPassVisible)}>
                    <Text style={{ color: "white" }}>Close</Text>
                  </TouchableOpacity>
                </View>
              </View> : null}
            </View>
          </View>
        </Modal>
        {/* ============================================================= */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => { console.log('Modal has been closed.'); }}>
          <View style={{
            flex: 1,
            width: "100%",
            height: "100%",
            position: "absolute",
            backgroundColor: "black",
            opacity: 0.5,
            top: 0,
            left: 0
          }} />
          <View style={{
            marginTop: Dimensions.get('screen').height * (1 / 3),
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <View style={styles.modalBox}>
              <View style={{ backgroundColor: '#FFB600', height: 50, borderTopRightRadius: 10, borderTopLeftRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20 }}>Fare Details</Text>
              </View>

              {this.state.carType == "WheelChair" ? // if (this.state.carType == "WheelChair")
                <View style={{ width: "100%", height: 170, justifyContent: "space-between" }}>
                  <Picker
                    selectedValue={this.state.showWheelchairPrice}
                    style={{ height: Platform.OS === 'ios' ? 170 : 50, width: "100%" }}
                    onValueChange={(itemValue, itemIndex) => {
                      if (itemValue == "WheelChairOne") {
                        this.setState({
                          showWheelchairPrice: itemValue,
                          baseFare: this.props.tripAmount.config.wheelchairOnePrice.baseFare,
                          PerKM: this.props.tripAmount.config.wheelchairOnePrice.farePerKm,
                          time: this.props.tripAmount.config.wheelchairOnePrice.farePerMin
                        });
                      } else {
                        this.setState({
                          showWheelchairPrice: itemValue,
                          baseFare: this.props.tripAmount.config.wheelchairTwoPrice.baseFare,
                          PerKM: this.props.tripAmount.config.wheelchairTwoPrice.farePerKm,
                          time: this.props.tripAmount.config.wheelchairTwoPrice.farePerMin
                        });
                      }
                    }}>
                    <Picker.Item label="WheelChairOne" value="WheelChairOne" />
                    <Picker.Item label="WheelChairTwo" value="WheelChairTwo" />
                  </Picker>
                  <View style={{
                    justifyContent: 'space-around',
                    height: 150,
                    padding: 20
                  }}>
                    {/* <Text style={{ fontWeight: 'bold', fontSize: 22 }}>{this.state.showWheelchairPrice}</Text> */}
                    <Text>Base Fare <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.baseFare}</Text></Text>
                    <Text>Per Km charge <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.PerKM}</Text></Text>
                    <Text>Per Min charges <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.time}</Text></Text>
                  </View>
                </View>
                :     //else if (this.state.carType == "Maxi")
                this.state.carType == "Maxi" ?
                  <View style={{ width: "100%", height: 170, justifyContent: "space-between" }}>
                    <Picker
                      selectedValue={this.state.showMaxiPrice}
                      style={{ height: Platform.OS === 'ios' ? 170 : 50, width: "100%" }}
                      onValueChange={(itemValue, itemIndex) => {
                        this.changeFareTypeMaxi(itemValue)
                      }}>
                      <Picker.Item label="FIVE" value="MaxiFIVE" />
                      <Picker.Item label="SIX" value="MaxiSIX" />
                      <Picker.Item label="SEVEN" value="MaxiSEVEN" />
                      <Picker.Item label="EIGHT" value="MaxiEIGHT" />
                      <Picker.Item label="NINE" value="MaxiNINE" />
                      <Picker.Item label="TEN" value="MaxiTEN" />
                      <Picker.Item label="ELEVEN" value="MaxiELEVEN" />
                    </Picker>
                    <View style={{
                      justifyContent: 'space-around',
                      height: 150,
                      padding: 20
                    }}>
                      {/* <Text style={{ fontWeight: 'bold', fontSize: 22 }}>{this.state.showWheelchairPrice}</Text> */}
                      <Text>Base Fare <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.baseFare}</Text></Text>
                      <Text>Per Km charge <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.PerKM}</Text></Text>
                      <Text>Per Min charges <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.time}</Text></Text>
                    </View>
                  </View>
                  :             //else
                  <View style={{
                    justifyContent: 'space-around',
                    height: 150,
                    paddingLeft: 20
                  }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 22 }}>{this.state.carType}</Text>
                    <Text>Base Fare <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.baseFare}</Text></Text>
                    <Text>Per Km charge <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.PerKM}</Text></Text>
                    <Text>Per Min charges <Text>: {_.get(this.props.appConfig, "quickestPrice.currencySymbol", "$")} {this.state.time}</Text></Text>
                  </View>}
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
                style={{ backgroundColor: '#FFB600', height: 30, borderBottomRightRadius: 10, borderBottomLeftRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20 }}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <StatusBar barStyle="light-content" />
        <View style={styles.slideSelector}>
          <View style={{
            // top: Dimensions.get('screen').height - 380,
            alignItems: "flex-end",
            justifyContent: 'flex-end',
            marginRight: 20
          }}>
            <TouchableOpacity
              style={{ flexDirection: "row", flex: 1, backgroundColor: 'transparent', marginBottom: 20 }}
              onPress={() => this.props.currentLocation()}
            >
              <View style={styles.locationPinBg}>
                <Icon
                  name="ios-locate-outline"
                  style={{ fontSize: 40, color: "#000", backgroundColor: 'transparent' }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <ScrollView style={{ width: "100%", height: 100, backgroundColor: "transparent", }} bounces={false} showsHorizontalScrollIndicator={false} horizontal={true}>
            <View style={{ width: 100, height: 100, backgroundColor: "transparent" }}>
              <View style={{ flex: 3, backgroundColor: "transparent", justifyContent: "flex-end" }}>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} style={[styles.detailBox, { height: this.state.QuickestFare }]}>
                  <Text style={{ backgroundColor: "transparent", paddingTop: 5, textAlignVertical: "center", height: this.state.QuickestFare }}>Detail</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.CarQuickest();
                  this.props.changeCarType("Quickest")
                  this.setState({ carType: "Quickest" })
                }}
                style={{ flex: 7, justifyContent: "center", alignItems: "center", backgroundColor: "white" }}>
                <Image
                  source={this.state.carType == "Quickest" ? require('../../../../assets/CarType1/car1-selected.png') : require('../../../../assets/CarType1/car1.png')}
                  style={styles.cars}
                />
                <Text>Quickest</Text>
              </TouchableOpacity>

            </View>
            <View style={{ width: 100, height: 100, backgroundColor: "transparent" }}>
              <View style={{ flex: 3, backgroundColor: "transparent", justifyContent: "flex-end" }}>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} style={[styles.detailBox, { height: this.state.SedanFare }]}>
                  <Text style={{ backgroundColor: "transparent", paddingTop: 5, textAlignVertical: "center", height: this.state.SedanFare }}>Detail</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.CarSedan();
                  this.props.changeCarType("Sedan")
                  this.setState({ carType: "Sedan" })
                }}
                style={{ flex: 7, justifyContent: "center", alignItems: "center", backgroundColor: "white" }}>
                <Image
                  source={this.state.carType == "Sedan" ? require('../../../../assets/CarType1/car2-selected.png') : require('../../../../assets/CarType1/car2.png')}
                  style={styles.cars}
                />
                <Text>Sedan</Text>
              </TouchableOpacity>

            </View>
            <View style={{ width: 100, height: 100, backgroundColor: "transparent" }}>
              <View style={{ flex: 3, backgroundColor: "transparent", justifyContent: "flex-end" }}>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} style={[styles.detailBox, { height: this.state.LuxuryFare }]}>
                  <Text style={{ backgroundColor: "transparent", paddingTop: 5, textAlignVertical: "center", height: this.state.LuxuryFare }}>Detail</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.CarLuxury();
                  this.props.changeCarType("Luxury")
                  this.setState({ carType: "Luxury" })
                }}
                style={{ flex: 7, justifyContent: "center", alignItems: "center", backgroundColor: "white" }}>
                <Image
                  source={this.state.carType == "Luxury" ? require('../../../../assets/CarType1/car3-selected.png') : require('../../../../assets/CarType1/car3.png')}
                  style={styles.cars}
                />
                <Text>Luxury</Text>
              </TouchableOpacity>

            </View>
            <View style={{ width: 100, height: 100, backgroundColor: "transparent" }}>
              <View style={{ flex: 3, backgroundColor: "transparent", justifyContent: "flex-end" }}>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} style={[styles.detailBox, { height: this.state.StationWagonFare }]}>
                  <Text style={{ backgroundColor: "transparent", paddingTop: 5, textAlignVertical: "center", height: this.state.StationWagonFare }}>Detail</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.CarStationWagon();
                  this.props.changeCarType("StationWagon")
                  this.setState({ carType: "StationWagon" })
                }}
                style={{ flex: 7, justifyContent: "center", alignItems: "center", backgroundColor: "white" }}>
                <Image
                  source={this.state.carType == "StationWagon" ? require('../../../../assets/CarType1/car4-selected.png') : require('../../../../assets/CarType1/car4.png')}
                  style={styles.cars}
                />
                <Text>StationWagon</Text>
              </TouchableOpacity>

            </View>
            <View style={{ width: 100, height: 100, backgroundColor: "transparent" }}>
              <View style={{ flex: 3, backgroundColor: "transparent", justifyContent: "flex-end" }}>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} style={[styles.detailBox, { height: this.state.WheelChairFare }]}>
                  <Text style={{ backgroundColor: "transparent", paddingTop: 5, textAlignVertical: "center", height: this.state.WheelChairFare }}>Detail</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setModalPassVisible(true);
                  this.CarWheelChair();
                  this.setState({ carType: "WheelChair" })
                }}
                style={{ flex: 7, justifyContent: "center", alignItems: "center", backgroundColor: "white" }}>
                <Image
                  source={this.state.carType == "WheelChair" ? require('../../../../assets/CarType1/car5-selected.png') : require('../../../../assets/CarType1/car5.png')}
                  style={styles.cars}
                />
                <Text>WheelChair</Text>
              </TouchableOpacity>

            </View>
            <View style={{ width: 100, height: 100, backgroundColor: "transparent" }}>
              <View style={{ flex: 3, backgroundColor: "transparent", justifyContent: "flex-end" }}>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} style={[styles.detailBox, { height: this.state.MaxiFare }]}>
                  <Text style={{ backgroundColor: "transparent", paddingTop: 5, textAlignVertical: "center", height: this.state.MaxiFare }}>Detail</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setModalPassVisible(true);
                  this.CarMaxi();
                  this.props.changeCarType("Maxi")
                  this.setState({ carType: "Maxi" })
                }}
                style={{ flex: 7, justifyContent: "center", alignItems: "center", backgroundColor: "white" }}>
                <Image
                  source={this.state.carType == "Maxi" ? require('../../../../assets/CarType1/car6-selected.png') : require('../../../../assets/CarType1/car6.png')}
                  style={styles.cars}
                />
                <Text>Maxi</Text>
              </TouchableOpacity>

            </View>
          </ScrollView>
          <SafeAreaView style={{ backgroundColor: '#FFB600' }}>
            <Button
              style={{
                flex: 1,
                backgroundColor: "#FFB600",
                borderRadius: 0,
                height: 50
              }}
              block
              large
              onPress={() => { this.setLocationClicked(); }}>
              <Text style={{ color: "#fff", fontSize: 18, fontWeight: "500" }}>Book Ride</Text>
            </Button>
          </SafeAreaView>
        </View>
        <View style={styles.headerContainer} pointerEvents="box-none">
          <SafeAreaView style={{ backgroundColor: '#FFB600' }}>
            <Header
              iosBarStyle="light-content"
              style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
              androidStatusBarColor="#FFB600">
              <Left>
                <Button transparent onPress={this.props.openDrawer}>
                  <Icon
                    name="ios-menu"
                    style={{ color: '#fff' }}
                  />
                </Button>
              </Left>
              <Body >
                <Title style={{ color: "#fff", fontSize: 20 }}>Kabbie</Title>
              </Body>
              <Right />
            </Header>
          </SafeAreaView>
          <Card
            style={
              Platform.OS === "ios" ? styles.iosSearchBar : styles.aSearchBar
            }
          >
            <Grid>
              <Col style={{}}>
                <Row
                  style={
                    Platform.OS === "ios"
                      ? { paddingLeft: 10, marginBottom: 8 }
                      : { paddingLeft: 10, left: -8 }
                  }
                >
                  <Text
                    style={{
                      ...styles.SearchPickText,
                      fontWeight: "bold",
                      marginBottom: 5,
                      paddingLeft: 10
                    }}
                  >
                    Pickup Location
                  </Text>
                </Row>
                <Row
                  style={
                    Platform.OS === "android"
                      ? { marginTop: -20, left: -10 }
                      : { marginTop: -30, paddingLeft: 5, left: -10 }
                  }
                >
                  <Item
                    style={
                      Platform.OS === "android"
                        ? {
                          flex: 1,
                          alignItems: "center",
                          borderColor: "transparent",
                          paddingBottom: 5,
                          paddingLeft: 10
                        } : {
                          flex: 1,
                          alignItems: "center",
                          borderColor: "transparent",
                          paddingTop: 7,
                          paddingLeft: 10
                        }
                    }>
                    <Text
                      numberOfLines={1}
                      style={{
                        padding: 5,
                        paddingLeft: 10,
                        fontSize: 15,
                      }}>
                      {_.get(this.props, "currentAddress", "")}
                    </Text>
                    {/* <Input
                      ref="searchInput"
                      placeholder="Enter Pickup Location"
                      onFocus={() => {
                        Actions.suggestLocation({
                          heading: "Starting Location",
                          page: "home"
                        });
                      }}
                      value={_.get(this.props, "currentAddress", "")}
                      editable={false}
                      placeholderTextColor={commonColor.lightThemePlaceholder}
                      style={{
                        top: Platform.OS === "ios" ? -10 : 2,
                        fontSize: 15,
                        marginHorizontal: 10,
                        paddingLeft: 10
                      }}
                    /> */}
                  </Item>
                </Row>
              </Col>
            </Grid>
          </Card>


        </View>

      </View>
    )
  }
}


function bindActions(dispatch) {
  return {
    changeCarType: (type) => dispatch(changeCarType(type)),
    openDrawer: () => dispatch(openDrawer()),
    setSourceLocation: () => dispatch(setSourceLocation()),
    changeRegion: region => dispatch(changeRegion(region)),
    changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    fetchUserCurrentLocationAsync: () => dispatch(fetchUserCurrentLocationAsync()),
    currentLocation: () => dispatch(currentLocation()),
    fetchAddressFromCoordinatesAsync: region => dispatch(fetchAddressFromCoordinatesAsync(region))
  };
}
export default connect(mapStateToProps, bindActions)(Home);
