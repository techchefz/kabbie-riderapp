import commonColor from "../../../../native-base-theme/variables/commonColor";

const React = require("react-native");

const { Dimensions, Platform } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
export default {
  iosSearchBar: {
    width: deviceWidth - 20,
    alignSelf: "center",
    marginTop: 10,
    flex: 1,
    height: 60,
    paddingTop: 12,
    margin: 10
  },
  aSearchBar: {
    width: deviceWidth - 20,
    alignSelf: "center",
    marginTop: 10,
    flex: 1,
    height: 60,
    paddingTop: 12,
    margin: 10
  },
  container: {
    flex: 1,
    position: "relative",
    backgroundColor: "#fff"
  },
  slideSelector: {
    backgroundColor: "transparent",
    position: "absolute",
    bottom: 0,
    width: deviceWidth,
  },

  map: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "#fff"
  },
  carIcon: {
    color: "#222",
    fontSize: 24
  },
  pinContainer: {
    bottom: deviceHeight / 2.1,
    position: "absolute",
    left: 0,
    right: 0
  },
  pinButton: {
    backgroundColor: '#2491CA',
    alignSelf: "center",
    paddingLeft: 50,
    paddingRight: 50,
  },
  pin: {
    width: 2,
    height: 15,
    backgroundColor: commonColor.brandPrimary,
    position: "relative",
    alignSelf: "center"
  },
  shareContainer: {
    borderBottomWidth: 1,
    borderBottomColor: "#ccc"
  },
  shareOptions: {
    paddingLeft: 20,
    padding: 10,
    backgroundColor: "#fff",
    alignItems: "flex-start"
  },
  shareType: {
    fontSize: 12,
    color: commonColor.lightThemeColor
  },
  share: {
    paddingRight: 10,
    padding: 10,
    alignItems: "flex-end"
  },
  taxiTypeContainer: {
    padding: 15,
    alignItems: "center"
  },
  taxiType: {
    opacity: 0.5,
    alignItems: "center"
  },
  taxi: {
    borderRadius: 18,
    borderWidth: 2,
    height: 36,
    width: 36,
    alignItems: "center",
    justifyContent: "center",
    borderColor: commonColor.lightThemeColor
  },
  taxiIcon: {
    fontSize: 15,
    padding: 5
  },
  headerContainer: {
    position: "absolute",
    top: 0,
    width: deviceWidth,
  },
  iosHeader: {
    backgroundColor: "#FFB600"
  },
  aHeader: {
    backgroundColor: "#FFB600",
    borderColor: "#aaa",
    elevation: 3,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  SearchPickText: {
    lineHeight: 18,
    fontSize: 14,
    color: "#B6D7EA",
    marginBottom: -2
  },
  selectedTaxi: {
    fontSize: 25,
    color: commonColor.brandPrimary,
    backgroundColor: "transparent",
    borderColor: commonColor.lightThemeColor
  },
  locateIcon: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    marginRight: 5,
    marginTop: Platform.OS === "ios" ? deviceHeight - 180 : deviceHeight - 200,
    marginBottom: 135,
    marginLeft: deviceWidth - 45
  },
  modal: {
    justifyContent: "center",
    alignItems: "center",
    height: 300,
    width: 300
  },
  setPickupLocation: {
    position: "absolute",
    bottom: deviceHeight / 2.1,
    left: 0,
    right: 0,
  },
  triangle: {
    position: 'absolute',
    bottom: -15,
    left: 75,
    width: 10,
    height: 15,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 0,
    borderLeftWidth: 10,
    borderTopColor: 'white',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
  },
  opaqueView: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: Dimensions.get('screen').height,
    width: Dimensions.get('screen').width,
    backgroundColor: 'black',
    opacity: 0.6
  },
  modalBox: {
    height: 300,
    backgroundColor: 'white',
    width: "70%",
    justifyContent: 'space-between',
    borderRadius: 10,
    shadowColor: 'black',
    shadowOffset: { height: 2, width: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 3
  },
  locationIcon: {
    backgroundColor: 'white',
    marginRight: 10,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    width: 50,
    height: 50,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5
      },
      android: {
        elevation: 2
      }
    })
  },
  modal: {
    justifyContent: "center",
    alignItems: "center",
    height: 300,
    width: 300
  },
  carTypes: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    width: '100%',
    height: 100,
    borderTopColor: "#ddd",
    borderTopWidth: 1,
    // top: Dimensions.get('screen').height - 350,
    alignItems: 'center',
    justifyContent: 'space-around',
    ...Platform.select({
      android: { elevation: 2 },
      ios: {
        shadowColor: '#ddd',
        shadowOffset: { height: -1, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 5
      }
    }),
  },
  cars: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  carsselected: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  locationPinBg: {
    backgroundColor: 'white',
    width: 46,
    height: 46,
    borderRadius: 23,
    justifyContent: 'center',
    alignItems: 'center',
    ...Platform.select({
      android: { elevation: 2 },
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 5
      }
    }),
  },
  fareBoxContainer: {
    width: "100%",
    backgroundColor: "transparent",
    height: 20,
    flexDirection: "row",
  },
  fareBox: {
    flex: 1,
    backgroundColor: "white",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -2, width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5
      },
      android: {
        elevation: 2
      }
    })
  },
  fareText: {
    textAlign: "center",
    backgroundColor: "transparent",
  },
  detailBox: {
    justifyContent: "flex-end",
    alignItems: "center",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    width: "100%",
    height: 20,
    backgroundColor: "white",
    borderBottomColor: "#999",
    borderBottomWidth: 0.5,
    ...Platform.select({
      android: { elevation: 2 },
      ios: {
        shadowColor: '#999',
        shadowOffset: { height: -1, width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 1
      }
    }),
  }
};
