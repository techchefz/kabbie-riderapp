import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Platform, SafeAreaView, BackHandler, TouchableOpacity } from "react-native";
import FAIcon from "react-native-vector-icons/MaterialCommunityIcons";
import { ImagePicker } from "expo";
import {
  Container,
  Header,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Item,
  Title,
  Left,
  Right,
  Body,
  Spinner
} from "native-base";
import { Actions } from "react-native-router-flux";

import SettingsForm from "./form";
import {
  updateUserProfileAsync,
  updateUserProfilePicAsync
} from "../../../actions/rider/settings";

import styles from "./styles";

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.rider.appState.jwtAccessToken,
    fname: state.rider.user.fname,
    lname: state.rider.user.lname,
    email: state.rider.user.email,
    phoneNo: state.rider.user.phoneNo,
    profileUrl: state.rider.user.profileUrl,
    userDetails: state.rider.user,
    profileUpdating: state.rider.user.profileUpdating
  };
}
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submit: false,
      image: null
    };
  }
  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.3
    });
    if (!result.cancelled) {
      this.setState({ image: result.uri });
      let userData = Object.assign(this.props.userDetails, {
        localUrl: result.uri
      });
      this.props.updateUserProfilePicAsync(userData, "profile");
    } else {
      this.setState({ image: this.props.profileUrl });
    }
  };
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }

  backAndroid() {
    return false;
  }
  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SafeAreaView style={{ backgroundColor: "#FFB600" }} />
        <Header
          iosBarStyle="light-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{ fontSize: 28, color: "#fff" }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >
              My Profile
            </Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Card
            style={{
              marginTop: 0,
              marginRight: 0,
              paddingTop: 20,
              paddingBottom: 20,
              marginLeft: 0
            }}
          >
            <CardItem style={{ padding: 0 }}>
              <Item
                style={{ paddingRight: 25, borderBottomWidth: 0 }}
              >
                <TouchableOpacity
                  onPress={this._pickImage}
                  style={{
                    position: "absolute",
                    right: 0,
                    top: 0
                  }}>
                  <FAIcon name="pencil-circle-outline" size={30} />
                </TouchableOpacity>
                {this.props.profileUpdating ? (
                  <View
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 35,
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: "#eee"
                    }}
                  >
                    <Spinner />
                  </View>
                ) : (
                    <Thumbnail
                      source={{ uri: this.props.profileUrl }}
                      style={{ width: 70, height: 70, borderRadius: 35 }}
                    />
                  )}
              </Item>
              <View style={{ marginLeft: 20 }}>
                <Text
                  style={{
                    color: "#fff",
                    fontSize: 20,
                    fontWeight: "400",
                    color: "#0F517F"
                  }}
                >
                  {this.props.fname} {this.props.lname}
                </Text>
                <Text note style={{ color: "#6895B0" }}>
                  {this.props.email}
                </Text>
              </View>
            </CardItem>
          </Card>
          <SettingsForm
            phoneNo={this.props.phoneNo.substr(3, 12)}
            code={this.props.phoneNo.substr(0, 3)}
          />
        </Content>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    updateUserProfileAsync: userDetails =>
      dispatch(updateUserProfileAsync(userDetails)),
    updateUserProfilePicAsync: (userData, type) =>
      dispatch(updateUserProfilePicAsync(userData, type))
  };
}

export default connect(mapStateToProps, bindActions)(Settings);
