//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView, ImageBackground, TouchableOpacity, StatusBar, Platform } from 'react-native';
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';
import { Icon, Content } from "native-base";
import { getReferredUsers } from '../../../actions/rider/fetchReferredUser'
import config from '../../../../config';

function mapStateToProps(state) {
    return {
        myState: state,
        email: state.rider.user.email,
        myReferralCode: state.rider.user.referralCode,
        jwtAccessToken: state.rider.appState.jwtAccessToken
    }
}
function bindActions(dispatch) {
    return {
        getReferredUsers: (email, jwtAccessToken) => dispatch(getReferredUsers(email, jwtAccessToken)),
    }
}

// create a component
class ReferralDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myReferredUsersList: [],
        }
    }

    fetchReferredUsers(email, jwtAccessToken) {
        fetch(`${config.serverSideUrl}:${config.port}/api/users/fetchReferredUsers`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: jwtAccessToken,
            },
            body: JSON.stringify({ email: email })
        })
            .then(resp => resp.json())
            .then(res => {
                this.setState({ myReferredUsersList: res })
            })
    }
    onClaimClick(referredUser_ID) {
        fetch(`${config.serverSideUrl}:${config.port}/api/users/claimRewards`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.jwtAccessToken,
            },
            body: JSON.stringify({ email: this.props.email, _id: referredUser_ID })
        }).then(res => {
            this.fetchReferredUsers(this.props.email, this.props.jwtAccessToken)
        })
    }
    componentWillMount() {
        this.fetchReferredUsers(this.props.email, this.props.jwtAccessToken);
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor={"#fff"}/>
                <SafeAreaView style={{ backgroundColor: "#fff", width: "100%" }} />
                <ImageBackground
                    source={require("../../../../assets/images/referBG.png")}
                    style={{ width: "100%", height: "100%", opacity: 1 }} >
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()}
                            style={{ flex: 1, justifyContent: 'center', alignItems: "center" }}>
                            <Icon
                                onPress={() => Actions.pop()}
                                name="md-arrow-back"
                                style={{ fontSize: 28, color: "#000", flex: 1 }}
                            />
                        </TouchableOpacity>
                        <Text style={{ textAlign: "center", fontSize: 20, flex: 9 }}>My Referrals</Text>
                        <View style={{ flex: 1 }} />
                    </View>
                    <Content>
                        {this.state.myReferredUsersList.map((user, i) => {
                            return (
                                <View style={{ width: "100%", paddingLeft: 10, paddingRight: 10 }}>
                                    <View style={{ flexDirection: "row" }}>
                                        <Text style={{ fontSize: 18, marginBottom: 15, marginTop: 15, backgroundColor: "transparent" }}>
                                            {user.fname} {user.lname}
                                        </Text>
                                        {
                                            user.tripsTaken == 9 || user.tripsTaken < 9 ?
                                                <Text
                                                    style={styles.tripsTakenText}>
                                                    {user.tripsTaken}/10
                                                           </Text>
                                                : <Text
                                                    style={styles.tripsTakenText}>
                                                    10/10
                                                       </Text>

                                        }
                                    </View>
                                    <View style={styles.Box}>
                                        <View
                                            style={{
                                                height: "100%",
                                                width: user.tripsTaken * 10 + '%',
                                                maxWidth: '100%',
                                                borderTopRightRadius: user.tripsTaken === 10 || user.tripsTaken > 10 ? 5 : 15,
                                                borderBottomRightRadius: user.tripsTaken === 10 || user.tripsTaken > 10 ? 5 : 15,
                                                backgroundColor: "#66ff66",
                                            }}
                                        />
                                    </View>
                                    {user.tripsTaken == 10 || user.tripsTaken > 10 ?
                                        <TouchableOpacity
                                            onPress={() => this.onClaimClick(user._id)}
                                            style={styles.claimBtn}>
                                            <Text style={{
                                                textAlign: "center",
                                                backgroundColor: "transparent",
                                                color: "black",
                                            }}>
                                                Claim Reward</Text>
                                        </TouchableOpacity> : null}
                                </View>)
                        })}
                    </Content>
                </ImageBackground>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    header: {
        width: "100%",
        height: 30,
        backgroundColor: "transparent",
        flexDirection: "row",
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 30
    },
    Box: {
        borderWidth: 0.5,
        backgroundColor:"white",
        borderColor: "#000",
        height: 20,
        width: "100%",
        borderRadius: 5,
        padding: 2,
        marginBottom: 15,

    },
    tripsTakenText: {
        flex: 1,
        color: 'black',
        textAlign: 'right',
        paddingRight: 10,
        fontSize: 18,
        marginBottom: 15,
        marginTop: 15,
    },
    claimBtn: {
        backgroundColor: "gold",
        height: 25,
        borderRadius: 10,
        alignSelf: 'flex-end',
        width: 150,
        justifyContent: "center",
        alignItems: "center",
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 3,
                    height: 3
                },
                shadowRadius: 3,
                shadowOpacity: 0.5
            },
            android: {
                elevation: 2
            }
        }),
    }
});

//make this component available to the app
export default connect(mapStateToProps, bindActions)(ReferralDetails)
