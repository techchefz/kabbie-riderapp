import React, { Component } from 'react'
import { View, Platform, Dimensions, Image, TouchableOpacity, SafeAreaView, Share, BackHandler } from 'react-native'
import {
    Container,
    Header,
    Content,
    Text,
    Button,
    Icon,
    Thumbnail,
    Card,
    CardItem,
    Title,
    Left,
    Right,
    Body
} from "native-base";
import styles from "./styles";
import { connect } from 'react-redux';
import { Actions } from "react-native-router-flux";
import Ionicon from "react-native-vector-icons/Ionicons";
import MIcon from "react-native-vector-icons/MaterialIcons";
import EIcon from "react-native-vector-icons/Entypo";
import FIcon from "react-native-vector-icons/FontAwesome";
import { changePageStatus } from "../../../actions/rider/home";

function mapStateToProps(state) {
    return {
        myState: state,
        email: state.rider.user.email,
        myReferralCode: state.rider.user.referralCode,
        jwtAccessToken: state.rider.appState.jwtAccessToken
    }
}
function bindActions(dispatch) {
    return {
        getReferredUsers: (email, jwtAccessToken) => dispatch(getReferredUsers(email, jwtAccessToken)),
        changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    }
}

export class RefernEarn extends Component {

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", () =>
            this.backAndroid()
        ); // Remove listener
    }

    backAndroid() {
        this.props.changePageStatus("home");
        Actions.rootView();
    }

    shareTextMessage() {
        Share.share({
            message: 'Hey download the Kabbie app with my referral code :: ' + this.props.myReferralCode + ' and get chance to win D20 on every 10 rides by sharing your referral code'
        }).catch(err => console.log(err))
    }
    render() {
        // console.log('====================================')
        // console.log(this.props.myReferralCode)
        // console.log('====================================')
        return (
            <View>
                <SafeAreaView style={{ backgroundColor: "#FFB600" }}>
                    <Header
                        iosBarStyle="light-content"
                        androidStatusBarColor="#FFB600"
                        style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
                    >
                        <Left>
                            <Button transparent onPress={() => Actions.pop()}>
                                <Icon
                                    name="md-arrow-back"
                                    style={{ fontSize: 28, color: "#fff" }}
                                />
                            </Button>
                        </Left>
                        <Body>
                            <Title
                                style={
                                    Platform.OS === "ios"
                                        ? styles.iosHeaderTitle
                                        : styles.aHeaderTitle
                                }
                            >
                                Refer n Earn
            </Title>
                        </Body>
                        <Right />
                    </Header>
                </SafeAreaView>
                <View style={{ height: Dimensions.get("screen").height, backgroundColor: 'white' }}>
                    <Content style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ width: "100%", height: Dimensions.get("screen").height * (1 / 3) }}>
                                <Image
                                    source={require("../../../../assets/images/refer.png")}
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        resizeMode: "contain"
                                    }} />
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', marginTop: 25, fontWeight: "100", marginBottom: 10 }}>Share your referral code</Text>
                            <Text
                                selectable={true}
                                style={{ fontSize: 22, color: 'black', textAlign: 'center' }}>{this.props.myReferralCode}</Text>
                            <Text style={{ fontSize: 18, color: 'black', textAlign: 'left', marginLeft: 10, marginTop: 30, fontWeight: "100", textAlign: "center" }}>Start inviting your friends</Text>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this.shareTextMessage()}
                                    style={styles.shareBtn}>
                                    <Text
                                        style={styles.shareBtnText}>SHARE</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => Actions.refernearnDetail()}
                                    style={[styles.shareBtn, { width: 250 }]}>
                                    <Text
                                        style={styles.shareBtnText}>Show My Referrals</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Content>
                </View>
            </View>
        )
    }
}

export default connect(mapStateToProps, bindActions)(RefernEarn)