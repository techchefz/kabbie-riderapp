import commonColor from '../../../../native-base-theme/variables/commonColor';

export default {
  iosHeader: {
    backgroundColor: '#FFB600',
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#fff",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#fff",
  },
  payCardInput: {
    flex: 2,
    paddingRight: 20,
  },

  container: {
    backgroundColor: '#F5F5F5',
    marginTop: 60,
  },
  label: {
    color: 'black',
    fontSize: 12,
  },
  input: {
    fontSize: 16,
    color: 'black',
  },
};
