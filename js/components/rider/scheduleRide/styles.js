import { Platform, Dimensions } from "react-native";

export default {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    heading: {
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize: 20,
    },
    backIcon: {
        backgroundColor:'transparent',
        fontSize:30,
        color:'#fff'
    },
    detailBox: {
        marginBottom:15,
        paddingTop:5,
        paddingLeft:10,
        paddingRight:10,
        paddingBottom:5,
        backgroundColor:'#fff',
        width: '100%',
        maxWidth: 350,
        borderRadius: 5,
        ...Platform.select({
            ios: {
                shadowColor: "black",
                shadowOffset: { height: 2, width: 2 },
                shadowRadius: 2,
                shadowOpacity: 0.5,
            },
            android: { elevation: 2 }
        }),
    },
    iconBox: {
        flex:1,
        backgroundColor:'green',
        width:50,
        height:50,
        marginLeft:10,
        marginRight:10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10,
        ...Platform.select({
            ios: {
                shadowColor: "black",
                shadowOffset: { height: 2, width: 2 },
                shadowRadius: 2,
                shadowOpacity: 0.5,
            },
            android: { elevation: 2 }
        }),
    },
    cardTitle: {
        fontWeight: "bold",
        marginBottom:2,
        backgroundColor:"transparent"
    },
    cardSubTitle: {
        backgroundColor:"transparent",
        fontWeight: "200",
    }
};
