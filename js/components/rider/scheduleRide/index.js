import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, Easing, 
    TouchableOpacity, Animated, Dimensions, Image, 
    Button, Alert, BackHandler, SafeAreaView } from 'react-native';
import { Header, Left, Right, Body, Content } from "native-base";
import Ionicon from "react-native-vector-icons/Ionicons";
import Ficon from "react-native-vector-icons/Feather";
import FAicon from "react-native-vector-icons/FontAwesome";
import Eicon from "react-native-vector-icons/Entypo";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import platform from '../../../../native-base-theme/variables/platform';
import { connect } from 'react-redux';
import Communications from "react-native-communications";
import config from '../../../../config';
import Spinner from "../../loaders/Spinner";
import { changePageStatus } from "../../../actions/rider/home";

class ScheduleRide extends Component {
    state = {
        animation: new Animated.Value(100),
        allTrips: [],
        loader: false
    }
    startAnimation = () => {
        Animated.timing(this.state.animation, {
            toValue: 200,
            duration: 1000,
            easing: Easing.elastic(5),
        }).start();
    }

    getAllTrips() {
        this.setState({ loader: true })
        fetch(`${config.serverSideUrl}:${config.port}/api/trips/getMyRides`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.jwtAccessToken,
                user_id: this.props.userId,
            }
        })
            .then(resp => resp.json())
            .then(res => {
                this.setState({ allTrips: res.data, loader: false })
            })
    }

    onCancelClick(tripId) {
        this.setState({ loader: true })

        fetch(`${config.serverSideUrl}:${config.port}/api/trips/getMyRides`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.jwtAccessToken,
            },
            body: JSON.stringify({ tripId: tripId, changeTripStatus: 'cancelled' })
        })
            .then(resp => resp.json())
            .then(res => {
                this.getAllTrips()

            })
    }

    componentWillMount() {
        this.getAllTrips()
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", () =>
            this.backAndroid()
        ); // Remove listener
    }

    backAndroid() {
        this.props.changePageStatus("home");
        Actions.rootView();
    }
    render() {

        const animatedStyles = {
            height: this.state.animation,
        }
        if (this.state.loader) {
            return (

                <View style={[styles.container, marginTop = 15]}>
                <SafeAreaView style={{backgroundColor:"#FFB600", width:"100%"}}>
                    <Header
                        androidStatusBarColor="#FFB600"
                        iosBarStyle="light-content"
                        style={{ width: '100%', backgroundColor: '#FFB600' }}>
                        <Left style={{ flex: 1 }}>
                            <Ionicon name="md-arrow-back"
                                onPress={() => Actions.pop()}
                                style={styles.backIcon} />
                        </Left>
                        <Body style={{ flex: 8 }}>
                            <Text style={styles.heading}>Schedule Rides</Text>
                        </Body>
                        <Right style={{ flex: 1 }} />
                    </Header>
                    </SafeAreaView>
                    <Content>
                        <View style={{ width: "100%", height: "100%", alignItems: "center", justifyContent: "center" }}>
                            <Spinner style={{ flex: 1, alignItems: "center" }} />
                        </View>
                    </Content>

                </View>
            )
        } else {
            return (
                <View style={styles.container}>
                <SafeAreaView style={{backgroundColor:"#FFB600", width:"100%"}}>
                    <Header
                        androidStatusBarColor="#FFB600"
                        iosBarStyle="light-content"
                        style={{ width: '100%', backgroundColor: '#FFB600' }}>
                        <Left style={{ flex: 1 }}>
                            <Ionicon name="md-arrow-back"
                                onPress={() => Actions.pop()}
                                style={styles.backIcon} />
                        </Left>
                        <Body style={{ flex: 8 }}>
                            <Text style={styles.heading}>Schedule Rides</Text>
                        </Body>
                        <Right style={{ flex: 1 }} />
                    </Header>
                    </SafeAreaView>
                    <Content
                        showsVerticalScrollIndicator={false}
                        style={{
                            flex: 9,
                            backgroundColor: '#e4e4e4',
                            width: '100%',
                            padding: 10
                        }}>
                        {
                            this.state.allTrips.length === 0 || undefined || null ? (
                                <View>
                                    <Text style={styles.cardTitle}>You have no scheduled trips</Text>
                                    <Image source={require('../../../../assets/images/scheduleTrip.png')}
                                        style={{
                                            width: Dimensions.get("screen").width,
                                            height: 300,
                                            resizeMode: "contain"
                                        }} />
                                </View>
                            ) : (
                                    this.state.allTrips.map((trip, index) => {
                                        return (
                                            < TouchableOpacity
                                                style={{ justifyContent: 'center', alignItems: 'center' }}
                                                onPress={this.startAnimation}>
                                                <View style={[styles.detailBox]}>
                                                    {trip.tripRequestStatus == "cancelled" ?
                                                        <Image
                                                            source={require('../../../../assets/images/cancelLogo.png')}
                                                            style={{
                                                                width: 200,
                                                                height: 200,
                                                                resizeMode: 'contain',
                                                                position: "absolute",
                                                                left: 80,
                                                                opacity: 0.2
                                                            }} />
                                                        : null}
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                                        <Image
                                                            source={require('../../../../assets/images/Scab.png')}
                                                            style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                                                        <Eicon name="dots-three-horizontal" style={{ color: 'green' }} />
                                                        <Eicon name="dots-three-horizontal" style={{ color: 'red' }} />
                                                        <Eicon name="dots-three-horizontal" style={{ color: 'green' }} />
                                                        <Eicon name="dots-three-horizontal" style={{ color: 'red' }} />
                                                        <Image
                                                            source={require('../../../../assets/images/Sflag.png')}
                                                            style={{ width: 30, height: 30, resizeMode: 'contain' }} />
                                                    </View>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={{ flex: 1, justifyContent: 'space-around', alignItems: 'center' }}>
                                                            <FAicon name="dot-circle-o" style={{ color: 'green' }} />
                                                            <Eicon name="dots-three-vertical" style={{ color: 'green' }} />
                                                            <FAicon name="dot-circle-o" style={{ color: 'red' }} />
                                                        </View>
                                                        <View style={{ flex: 9 }}>
                                                            <Text style={styles.cardTitle} numberOfLines={2}>Source :
                                                 <Text style={styles.cardSubTitle}> {trip.pickUpAddress}</Text>
                                                            </Text>
                                                            <Text style={styles.cardTitle} numberOfLines={2}>Destination :
                                                 <Text style={styles.cardSubTitle}> {trip.destAddress}</Text>
                                                            </Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ paddingLeft: 10, flexDirection: 'row' }}>
                                                        <View style={{ flex: 1.3 }}>
                                                            <Text style={styles.cardTitle}>Scheduled Time </Text>
                                                            <Text style={styles.cardTitle}>Scheduled Date </Text>
                                                            <Text style={styles.cardTitle}>Booking Date/Time </Text>
                                                        </View>
                                                        <View style={{ flex: 1.5 }}>
                                                            <Text style={styles.cardSubTitle}>: {trip.scheduleOn.slice(11, 16)} </Text>
                                                            <Text style={styles.cardSubTitle}>: {trip.scheduleOn.slice(0, 10)} </Text>
                                                            <Text style={styles.cardSubTitle}>: {trip.requestTime.slice(0, 10)} / {trip.requestTime.slice(11, 16)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{
                                                        flex: 1,
                                                        marginTop: 10,
                                                        marginBottom: 10,
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-around',
                                                        width: "100%"
                                                    }}>
                                                        {trip.tripRequestStatus == "cancelled" ? null :
                                                            <TouchableOpacity
                                                                style={[styles.iconBox, { backgroundColor: '#ff5c33', flexDirection: 'row' }]}
                                                                onPress={() => Alert.alert('WARNING',
                                                                    'Are you sure, you want to cancel the trip',
                                                                    [
                                                                        { text: 'Yes', onPress: () => this.onCancelClick(trip._id) },
                                                                        { text: 'No', style: 'cancel' },
                                                                    ],
                                                                    { cancelable: false })}>
                                                                <Ionicon name="md-close"
                                                                    style={{ fontSize: 24, color: "white" }} />
                                                                <Text style={{ color: "white", marginLeft: 20 }}>Cancel Trip</Text>
                                                            </TouchableOpacity>
                                                        }
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        );
                                    })
                                )
                        }

                    </Content>
                </View >
            );
        }
    }
}
function mapStateToProps(state) {
    return {
        jwtAccessToken: state.rider.appState.jwtAccessToken,
        userId: state.rider.user._id,
    }
}
function bindActions(dispatch) {
    return {
      fetchTripHistoryAsync: jwtAccessToken =>
        dispatch(fetchTripHistoryAsync(jwtAccessToken)),
      changePageStatus: newPage => dispatch(changePageStatus(newPage)),      
    };
  }
export default connect(mapStateToProps, bindActions)(ScheduleRide);
