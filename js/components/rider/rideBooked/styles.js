import commonColor from '../../../../native-base-theme/variables/commonColor';
import { Platform } from "react-native";
const React = require('react-native');

const { Dimensions } = React;
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default {
  searchBar: {
    width: deviceWidth,
    alignSelf: 'center',
    height: 50,
    borderRadius: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  srcdes: {
    flex: 1,
    top: -8,
    width: deviceWidth + 5,
  },


  slideSelector: {
    // paddingBottom: 10,
    // backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    width: deviceWidth + 5,
    paddingHorizontal: 0,
  },

  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  driverInfoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  Otp: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    width: 60
  },
  driverInfo: {
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1,
    flex: 1,
    padding: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    marginTop: -10,
    borderColor: commonColor.lightThemeColor,
  },
  carInfo: {
    borderWidth: 1,
    padding: 3,
    backgroundColor: '#eee',
    marginTop: -10,
    borderColor: commonColor.lightThemeColor,
  },
  card: {
    alignItems: 'center',
    // borderWidth: 1,
    flex: 2,
    // borderColor: '#EEE',
    height: 55,
    borderRadius: 0,
    // paddingTop: 12,
    // paddingBottom: 8,
    paddingHorizontal: 8,
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 16,
    lineHeight: 15,
    fontWeight: 'bold'
  },
  waitTime: {
    textAlign: 'center',
    fontSize: 10,
    lineHeight: 15,
    marginTop: 10,
  },
  headerContainer: {
    position: 'absolute',
    top: 0,
    width: deviceWidth + 5,
  },
  iosHeader: {
    backgroundColor: '#FFB600',
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#000',
    elevation: 3,
  },
  confirmation: {
    textAlign: 'center',
    marginTop: -3,
    fontSize: 14,
    fontWeight: 'bold',
  },

  modalTopContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalTextViewContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  modalText: {
    alignSelf: 'center',
  },
  locateIcon: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    alignSelf: 'flex-end',
    marginRight: 5,
    marginTop: Platform.OS === "ios" ? deviceHeight - 240 : deviceHeight - 280,
    // marginBottom: 145,
    // marginLeft: deviceWidth - 135
  },
  confirmBtn: {
    flex: 1,
    alignSelf: "center",
    backgroundColor: "green",
    width: "90%",
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: {
          width: 3,
          height: 3
        },
        shadowRadius: 3,
        shadowOpacity: 0.5
      },
      android: {
        elevation: 2
      }
    }),
  }
};
