import * as Expo from "expo";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Platform,
  Modal,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  NetInfo
} from "react-native";
import {
  Header,
  Text,
  Button,
  Icon,
  Card,
  CardItem,
  Title,
  Thumbnail,
  Right,
  Left,
  Body,
  Toast,
  Spinner,
  Grid,
  Col,
  Row
} from "native-base";
import Communications from "react-native-communications";
import _ from "lodash";
import PropTypes from "prop-types";
import * as tripViewSelector from "../../../reducers/rider/tripRequest";
import { changePageStatus, currentLocation } from "../../../actions/rider/home";
import { cancelRide } from "../../../actions/rider/rideBooked";
import { Actions, ActionConst } from "react-native-router-flux";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import ModalView from "../../common/ModalView";
import { fetchCurrentFareDetail } from "../../../actions/rider/confirmRide";
import { changeDestination } from "../../../services/ridersocket";
import { confirmBtn } from "../../../actions/rider/changeDestination";
import FIcon from "react-native-vector-icons/Feather";

import { updateLocation } from "../../../services/ridersocket";
import { setUserLocation } from "../../../actions/rider/home";
// import { setCurrentMap } from "../../../components/rider/rootView";

const deviceWidth = Dimensions.get("window").width;

const profileImage = require("../../../../assets/images/Contacts/avatar-9.jpg");
const taxiIcon = require("../../../../assets/images/Taxi-Icon.jpg");
const driverIcon = require("../../../../assets/images/Taxi-cap1.jpg");

function mapStateToProps(state) {
  return {
    previousSource: state,
    tripRequest: state.rider.tripRequest,
    trip: state.rider.trip,
    tripStatus: state.rider.trip.tripStatus,
    socketDisconnected: state.rider.appState.socketDisconnected,
    tripViewSelector: tripViewSelector.tripView(state),
    riderObj: state.rider.user,
    test: state
  };
}
class RideBooked extends Component {
  static propTypes = {
    tripRequest: PropTypes.object,
    cancelRide: PropTypes.func,
    changePageStatus: PropTypes.func,
    socketDisconnected: PropTypes.bool,
    tripViewSelector: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      confirmBtn: false,
      stars: [
        { active: false },
        { active: false },
        { active: false },
        { active: false },
        { active: false }
      ]
    };
  }

  handlePress() {
    this.props.cancelRide();
  }
  goBack() {
    this.props.changePageStatus("home");
  }
  driverRating() {
    return <View style={{ flexDirection: "row" }} />;
  }

  onConfirmClick() {
    this.props.confirmBtn(false);
    changeDestination(this.props.tripRequest);
  }
  checkInternat() {
    // NetInfo.isConnected.fetch().then(isConnected => {
    //   isConnected ? this.netOnline() : 
    //   this.netOffline()
    //     // Toast.show({
    //     //   text:"Device Offline",
    //     //   buttonText: "Okay", })
    // });
    function handleFirstConnectivityChange(isConnected) {
      isConnected ? null :
        Toast.show({
          text: "Device Offline",
          duration: 5000,
          buttonText: "Okay"
        })
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        handleFirstConnectivityChange
      );
    }
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      handleFirstConnectivityChange
    );
  }
  render() {
    this.checkInternat()
    //DRIVER CONFIRMED AND ENROUTE
    //Driver is waiting at your pickup location.
    //YOU ARE ON TRIP

    return (
      <View pointerEvents="box-none" style={{ flex: 1 }}>
        {this.props.tripViewSelector.subText == "YOU ARE ON TRIP" ? null :
          <View style={styles.locateIcon}>
            <TouchableOpacity onPress={() => this.props.currentLocation()}>
              <Icon
                name="ios-locate-outline"
                style={{ fontSize: 40, color: commonColor.brandPrimary, backgroundColor: 'transparent' }}
              />
            </TouchableOpacity>
          </View>}
        {this.props.tripViewSelector.subText == "YOU ARE ON TRIP" ?
          <View style={{ width: "100%", height: "100%", marginTop: 0, position: "absolute" }}>
            <View style={{ backgroundColor: "transparent", width: "100%", bottom: 0, position: "absolute" }}>
              <View style={{ height: 50, width: "100%", backgroundColor: "transparent", alignItems: "flex-end", paddingRight: 20 }}>
                <TouchableOpacity onPress={() => this.props.currentLocation()}>
                  <Icon
                    name="ios-locate-outline"
                    style={{ fontSize: 40, color: commonColor.brandPrimary, backgroundColor: 'transparent' }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ backgroundColor: "white", width: "100%" }}>
                <View style={{ flexDirection: "row", borderBottomWidth: 0.5, borderBottomColor: "lightblue" }}>
                  <View style={{ flex: 1, padding: 10, justifyContent: "center", alignItems: "center" }}>
                    <Thumbnail
                      size={70}
                      source={{
                        uri: _.get(
                          this.props,
                          "tripRequest.driver.profileUrl",
                          "profileImage"
                        )
                      }}
                      style={{
                        borderRadius: 28,
                        borderWidth: 1,
                        borderColor: "#EEE"
                      }}
                    />
                    <View style={styles.driverInfo}>
                      <View style={{ flexDirection: "row" }}>
                        {this.state.stars.map(
                          (item, index) =>
                            3 ? (
                              3 > index ? (
                                <Icon
                                  key={index}
                                  name="ios-star"
                                  style={{
                                    marginLeft: 0,
                                    marginRight: 3,
                                    fontSize: 15,
                                    width: null,
                                    color: commonColor.brandPrimary
                                  }}
                                />
                              ) : (
                                  <Icon
                                    key={index}
                                    name="ios-star-outline"
                                    style={{
                                      marginLeft: 0,
                                      width: null,
                                      marginRight: 3,
                                      fontSize: 15,
                                      // color: ""
                                    }}
                                  />
                                )
                            ) : null
                        )}
                      </View>
                    </View>
                    <Text>
                      {_.get(this.props, "tripRequest.driver.fname", "Driver")}
                    </Text>
                  </View>
                  <View style={{ flex: 1, padding: 10, justifyContent: "center", alignItems: "center" }}>
                    <Thumbnail
                      size={40}
                      source={taxiIcon}
                      style={{
                        borderRadius: 28,
                        borderWidth: 1,
                        borderColor: "#eee"
                      }}
                    />
                    <View style={styles.carInfo}>
                      <Text style={{ fontSize: 12, lineHeight: 13 }}>
                        {_.get(
                          this.props,
                          "tripRequest.driver.carDetails.regNo",
                          "NYC 603"
                        )}
                      </Text>
                    </View>
                    <Text style={{ fontSize: 12, lineHeight: 15 }}>
                      {_.get(
                        this.props,
                        "tripRequest.driver.carDetails.company",
                        "Cab"
                      )}
                    </Text>
                  </View>
                  {/* ===============================================Confirm Change================================================ */}
                  {this.props.test.rider.appState.confirmBtn == false ? null :
                    <View style={{ flex: 1, padding: 10, justifyContent: "center", alignItems: "center" }}>
                      <TouchableOpacity
                        onPress={() => this.onConfirmClick()}
                        style={{ backgroundColor: "green", padding: 10, borderRadius: 5 }}>
                        <Text style={{ textAlign: "center", color: "white" }}>Confirm{'\n'}Change</Text>
                      </TouchableOpacity>
                    </View>
                  }
                  {/* ===============================================Confirm Change================================================ */}
                </View>
                <View style={{ borderBottomWidth: 0.5, borderColor: "#666666", width: "100%", height: 40, justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ textAlign: "center" }}>{this.props.test.rider.tripRequest.destAddress}</Text>
                </View>
                <View>
                  <SafeAreaView style={{ backgroundColor: "#FFB600", }}>
                    <View style={{ flexDirection: "row", height: 50 }}>
                      <Button
                        block
                        style={{ ...styles.card, backgroundColor: "#FFB600", flex: 2 }}
                        onPress={() =>
                          Communications.phonecall(
                            _.get(this.props, "tripRequest.driver.phoneNo", ""),
                            true
                          )
                        }
                      >
                        <FIcon
                          name="phone-call"
                          size={30}
                          color="white" />
                      </Button>
                      <Button
                        block
                        style={{ ...styles.card, backgroundColor: "#fff", flex: 8 }}
                        onPress={() => {
                          this.props.confirmBtn(true),
                            Actions.suggestLocation({
                              heading: "Destination Location",
                              page: "destination"
                            });
                        }}
                      >
                        <Text style={{ ...styles.btnText, color: "#FF8A65", lineHeight: 30 }}>
                          Change drop location
                  </Text>
                      </Button>
                    </View>
                  </SafeAreaView>
                </View>
              </View>
            </View>
          </View>
          : null}
        {this.props.tripViewSelector.showFooter ? (
          <View style={styles.slideSelector}>
            <Card
              style={{ bottom: -5, borderBottomWidth: 1, borderColor: "#eee" }}
            >
              <CardItem>
                <Grid style={{ flexDirection: "row", borderWidth: 0 }}>
                  <Col style={styles.driverInfoContainer}>
                    <Thumbnail
                      size={70}
                      source={{
                        uri: _.get(
                          this.props,
                          "tripRequest.driver.profileUrl",
                          "profileImage"
                        )
                      }}
                      style={{
                        borderRadius: 28,
                        borderWidth: 1,
                        borderColor: "#EEE"
                      }}
                    />
                    <View style={styles.driverInfo}>
                      <View style={{ flexDirection: "row" }}>
                        {this.state.stars.map(
                          (item, index) =>
                            3 ? (
                              3 > index ? (
                                <Icon
                                  key={index}
                                  name="ios-star"
                                  style={{
                                    marginLeft: 0,
                                    marginRight: 3,
                                    fontSize: 15,
                                    width: null,
                                    color: commonColor.brandPrimary
                                  }}
                                />
                              ) : (
                                  <Icon
                                    key={index}
                                    name="ios-star-outline"
                                    style={{
                                      marginLeft: 0,
                                      width: null,
                                      marginRight: 3,
                                      fontSize: 15,
                                      // color: ""
                                    }}
                                  />
                                )
                            ) : null
                        )}
                      </View>
                    </View>
                    <Text>
                      {_.get(this.props, "tripRequest.driver.fname", "Driver")}
                    </Text>
                  </Col>
                  <Col style={styles.driverInfoContainer}>
                    <Thumbnail
                      size={40}
                      source={taxiIcon}
                      style={{
                        borderRadius: 28,
                        borderWidth: 1,
                        borderColor: "#eee"
                      }}
                    />
                    <View style={styles.carInfo}>
                      <Text style={{ fontSize: 12, lineHeight: 13 }}>
                        {_.get(
                          this.props,
                          "tripRequest.driver.carDetails.regNo",
                          "NYC 603"
                        )}
                      </Text>
                    </View>
                    <Text style={{ fontSize: 12, lineHeight: 15 }}>
                      {_.get(
                        this.props,
                        "tripRequest.driver.carDetails.company",
                        "Cab"
                      )}
                    </Text>
                  </Col>
                  <Col style={styles.Otp}>
                    <Text style={{ fontSize: 18, fontWeight: "bold", marginBottom: 10 }}>OTP</Text>
                    <Text>{this.props.tripRequest.tripOtp}</Text>
                  </Col>
                </Grid>
              </CardItem>
            </Card>
            <SafeAreaView style={{ backgroundColor: "#FFB600" }}>
              <View style={{ flexDirection: "row", height: 50 }}>
                <Button
                  block
                  style={{ ...styles.card, backgroundColor: "#FFB600" }}
                  onPress={() =>
                    Communications.phonecall(
                      _.get(this.props, "tripRequest.driver.phoneNo", ""),
                      true
                    )
                  }
                >
                  <Text style={styles.btnText}>Contact</Text>
                </Button>
                {this.props.tripViewSelector.cancelButton ? (
                  <Button
                    block
                    style={{ ...styles.card, backgroundColor: "#fff" }}
                    onPress={() => this.handlePress()}
                  >
                    <Text style={{ ...styles.btnText, color: "#FF8A65" }}>
                      Cancel
                  </Text>
                  </Button>
                ) : (
                    <View />
                  )}
              </View>
            </SafeAreaView>
            {/* <Text style={styles.waitTime}>
                PICKUP TIME IS APPROXIMATELY 2 MINUTES
              </Text> */}
          </View>
        ) : (
            <View />
          )}
        <View style={styles.headerContainer}>
          <SafeAreaView style={{ backgroundColor: "#FFB600" }} />
          <Header
            iosBarStyle="light-content"
            androidStatusBarColor="#FFB600"
            // androidStatusBarColor="#de7d17"
            style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
          >
            {this.props.tripViewSelector.backButton ? (
              <Left>
                <Button transparent onPress={() => this.goBack()}>
                  <Icon
                    name="md-arrow-back"
                    style={{ fontSize: 28, color: "#fff" }}
                  />
                </Button>
              </Left>
            ) : (
                <Left />
              )}
            <Body>
              <Title style={{ color: "#fff" }}>
                {_.get(this.props.tripViewSelector, "heading", "On Trip")}
              </Title>
            </Body>
            <Right />
          </Header>
          {/* change destination  */}
          <View style={styles.srcdes}>
            <Card style={styles.searchBar}>
              <CardItem style={{ borderWidth: 0, borderColor: "transparent" }}>
                <Text style={styles.confirmation}>
                  {_.get(
                    this.props.tripViewSelector,
                    "subText",
                    "You are on way to reach Destination"
                  )}
                </Text>
              </CardItem>
            </Card>
            {/* {this.props.tripViewSelector.subText == "YOU ARE ON TRIP" ?
              <View style={{ width: "100%" }}>
                <CardItem>
                  <Button
                    onPress={() => {
                      Actions.suggestLocation({
                        heading: "Destination Location",
                        page: "destination"
                      });
                    }}
                    transparent
                    style={{ flex: 1, paddingLeft: 10, fontSize: 16 }}
                  >
                    {this.props.destAddress !== "" ? (
                      <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                        {_.get(
                          this.props,
                          "destAddress",
                          "Enter Drop Location"
                        )}
                      </Text>
                    ) : (
                        <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                          Enter Drop Location
                      </Text>
                      )}
                  </Button>
                </CardItem>
                <TouchableOpacity
                  onPress={() => this.onConfirmClick()}
                  style={styles.confirmBtn}
                >
                  <Text style={{ color: "white" }}>Confirm</Text>
                </TouchableOpacity>
              </View>
              : null} */}
          </View>
        </View>
        {/*<Modal
          animationType={"none"}
          transparent
          visible={this.props.socketDisconnected}
        >
          <View style={styles.modalTopContainer}>
            <View style={styles.modalTextViewContainer}>
              <Text style={styles.modalText}>
                Internet is disconnected at the moment
              </Text>
            </View>
          </View>
      </Modal>*/}
      </View>
    );
  }
}

function bindActions(dispatch) {
  return {
    cancelRide: () => dispatch(cancelRide()),
    currentLocation: () => dispatch(currentLocation()),
    confirmBtn: (payload) => dispatch(confirmBtn(payload)),
    setUserLocation: (position) => dispatch(setUserLocation(position)),
    storePreviousSource: (previousSource) => dispatch(storePreviousSource(previousSource)),
    changePageStatus: newPage => dispatch(changePageStatus(newPage))
  };
}

export default connect(mapStateToProps, bindActions)(RideBooked);
