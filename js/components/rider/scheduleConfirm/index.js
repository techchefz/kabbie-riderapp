//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import MCIcon from "react-native-vector-icons/MaterialCommunityIcons";
import { Actions } from "react-native-router-flux";
import config from "../../../../config";
import { connect } from "react-redux";
import { changePageStatus } from "../../../actions/rider/home";


// create a component
function mapStateToProps(state) {
    return {
        pickUpAddress: state.rider.tripRequest.pickUpAddress,
        destAddress: state.rider.tripRequest.destAddress,
        rider: state.rider.user,
        jwtAccessToken: state.rider.appState.jwtAccessToken,
    };
}
class ScheduleConirm extends Component {
    onGotIt() {
        this.props.changePageStatus("home");
        Actions.rootView();
    }
    render() {
        return (
            <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor={"#fff"}/>
                <MCIcon
                    name="checkbox-multiple-marked-circle"
                    style={{
                        fontSize: 50,
                        color: "green"
                    }} />
                <Text style={{ fontSize: 25, marginBottom: 20 }}>Ride Scheduled</Text>
                <Text style={{ fontSize: 16, textAlign: "center", width: "80%", maxWidth: 350, fontWeight: "200" }}>
                    Your ride has been scheduled. Driver details will be shared 10 min before your pickup time.</Text>
                <View
                    style={{
                        borderBottomWidth: 0.5,
                        borderColor: "#afafaf",
                        width: "90%",
                        marginTop: 50
                    }}
                />
                <Text
                    style={{
                        marginTop: 30,
                        color: "blue",
                        fontSize: 18,
                        fontWeight: "100"
                    }}
                    onPress={() => this.onGotIt()}>
                    GOT IT</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
function bindActions(dispatch) {
    return {
        changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    };
}
export default connect(mapStateToProps, bindActions)(ScheduleConirm);
