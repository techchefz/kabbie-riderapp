import commonColor from '../../../../native-base-theme/variables/commonColor';

export default {
  iosHeader: {
    backgroundColor: '#FFB600',
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '600',
    color: "#fff",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '600',
    lineHeight: 26,
    marginTop: -5,
    color: "#fff",
  },
  cardSelect: {
    margin: 20,
    marginLeft: 20,
    padding: 10,
    marginTop: 0,
    paddingLeft: 0,
  },
  payCard: {
    flexDirection: 'row',
    paddingLeft: 20,
    marginTop: 0,
    paddingVertical: 10,
    backgroundColor: '#FFB600',
  },
  paytmIcon: {
    width: 35,
    height: 13,
    padding: 5,
    paddingTop: 15,
  },
};
