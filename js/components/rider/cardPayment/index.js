import React, { Component } from "react";
import { connect } from "react-redux";
import { Image, View, TouchableOpacity, Platform, SafeAreaView, BackHandler } from "react-native";
import {
  Container,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Body,
  Spinner
} from "native-base";
import { Actions, ActionConst } from "react-native-router-flux";
import { saveCardDetails } from "../../../actions/payment/riderCardPayment";
import { updatePayment } from "../../../actions/payment/paymentMethod";
import ModalView from "../../common/ModalView";
import config from "../../../../config";

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import { changePageStatus } from "../../../actions/rider/home";

const image = require("../../../../assets/images/paytm2.png");

class cardPayment extends Component {

  state = {
    walletBalance: 0,
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }

  backAndroid() {
    this.props.changePageStatus("home");
    Actions.rootView();
  }
  componentWillMount() {
    this.getWalletAmt(this.props.userEmail, this.props.jwtAccessToken);
  }
  getWalletAmt(userEmail, jwtAccessToken) {
    fetch(`${config.serverSideUrl}:${config.port}/api/users/claimRewards`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: jwtAccessToken,
        email: userEmail
      }
    })
      .then(resp => resp.json())
      .then(res => {
        this.setState({ walletBalance: res.data })
      })
  }
  onSubmit() {
    const userEmail = {
      email: this.props.userEmail
    };
    this.props.saveCardDetails(userEmail);
  }

  selectCash() {
    this.props.updatePayment("CASH");
  }

  showLoaderModal() {
    return (
      <ModalView>
        <Spinner />
      </ModalView>
    );
  }
  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SafeAreaView style={{ backgroundColor: "#FFB600" }}>
          <Header
            iosBarStyle="light-content"
            androidStatusBarColor="#FFB600"
            style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
          >
            <Left style={{
              justifyContent: "center",
              alignItems: "center",
              flex: 1
            }}>
              <Button transparent onPress={() => Actions.pop()}>
                <Icon
                  name="md-arrow-back"
                  style={{ fontSize: 28, color: "#fff" }}
                />
              </Button>
            </Left>
            <Body style={{
              flex: 8,
            }}>
              <Title
                style={
                  Platform.OS === "ios"
                    ? styles.iosHeaderTitle
                    : styles.aHeaderTitle
                }
              >
                Payment
            </Title>
            </Body>
            <Right style={{ flex: 1 }} />
          </Header>
        </SafeAreaView>
        <View>
          <View style={{
            borderBottomWidth: 0.5,
            borderBottomColor: "#afafaf",
            marginBottom: 20,
            marginTop: 40,
            width: "90%",
            alignSelf: "center"
          }}>
            <Text style={{ fontSize: 26, marginBottom: 10 }}>
              {this.props.appConfig.sedanPrice.currencySymbol} {this.state.walletBalance}
            </Text>
          </View>
          <View style={styles.cardSelect}>
            <Text
              style={{ fontSize: 14, fontWeight: "bold", color: "#000" }}
            >
              Select how you would like to pay
            </Text>
          </View>
          <View style={{ paddingHorizontal: 20 }}>
            {this.props.appConfig.stripe ? (
              <TouchableOpacity
                style={{
                  ...styles.payCard,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                  borderBottomLeftRadius: 4,
                  borderBottomRightRadius: 4,
                  marginBottom: 10,
                  borderBottomColor: "#fff"
                }}
                onPress={() => this.onSubmit()}
              >
                <Icon name="ios-card" style={{ fontSize: 40, color: "#fff" }} />
                <Text
                  style={{
                    marginLeft: 20,
                    marginTop: 8,
                    color: "#fff",
                    fontWeight: "bold"
                  }}
                >
                  Credit/Debit Card
                </Text>
              </TouchableOpacity>
            ) : null}
            {this.props.appConfig.cash ? (
              <TouchableOpacity
                style={{
                  ...styles.payCard,
                  borderBottomLeftRadius: 4,
                  borderBottomRightRadius: 4,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}
                onPress={() => this.selectCash()}
              >
                <Icon name="ios-cash" style={{ fontSize: 40, color: "#fff" }} />
                <Text
                  style={{
                    marginLeft: 20,
                    marginTop: 8,
                    color: "#eee",
                    fontWeight: "bold"
                  }}
                >
                  Cash
                </Text>
              </TouchableOpacity>
            ) : null}
            {this.props.ridePayment.cardDetailsLoader
              ? this.showLoaderModal()
              : null}
          </View>
        </View>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.rider.appState.jwtAccessToken,
    userEmail: state.rider.user.email,
    ridePayment: state.rider.rideCardPayment,
    appConfig: state.basicAppConfig.config,
    walletBalance: state.rider.user.wallet
  };
}

function bindActions(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    saveCardDetails: data => dispatch(saveCardDetails(data)),
    updatePayment: data => dispatch(updatePayment(data)),
    changePageStatus: newPage => dispatch(changePageStatus(newPage)),
  };
}

export default connect(mapStateToProps, bindActions)(cardPayment);
