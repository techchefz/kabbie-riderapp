import { Platform, Dimensions } from "react-native";

export default {
    heading: {
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize: 20,
    },
    subHeading: {
        color: '#000',
        backgroundColor: 'transparent',
        fontSize: 16,
        textAlign: 'center'
    },
    balance: {
        color: '#000',
        backgroundColor: 'transparent',
        fontSize: 16,
        fontSize: 40,
        textAlign: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    backView1: {
        height: Dimensions.get('screen').height,
        width: '100%',
        backgroundColor: 'lightblue'
    },
    backIcon: {
        backgroundColor: 'transparent',
        fontSize: 30,
        color: '#fff'
    },
    textInput: {
        height: 40,
        borderBottomWidth: 0.5,
        borderColor: "#afafaf",
        width: "100%",
        maxWidth: 300,
        marginBottom: 10
    },
    map: {
        height: 100,
        marginTop: 10,
    },
    radioContainer: {
        height: 200,
        justifyContent: 'center',
        paddingLeft: 20
        //  alignSelf:'center',
    },
    modalIOS: {
        width: 300,
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 3,
            height: 3
        },
        shadowRadius: 3,
        shadowOpacity: 0.5
    },
    modalIOSConfirm: {
        width: 100,
        marginTop:30,
        height:30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 3,
            height: 3
        },
        shadowRadius: 3,
        shadowOpacity: 0.5
    },
};
