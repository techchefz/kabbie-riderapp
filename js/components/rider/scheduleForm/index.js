//import liraries
import React, { Component } from 'react';
import {
    View, Text,
    StyleSheet, Dimensions,
    StatusBar, TouchableOpacity,
    Platform, Picker,
    TextInput, Modal,
    TouchableHighlight, Image
} from 'react-native';
import config from "../../../../config";
import { Header, Left, Right, Body, Content, Button } from "native-base";
import { connect } from "react-redux";
import Ionicon from "react-native-vector-icons/Ionicons";
import FIcon from "react-native-vector-icons/FontAwesome";
import EIcon from "react-native-vector-icons/Entypo";
import { changePageStatus } from "../../../actions/rider/home";
import styles from "./styles";
import DatePicker from 'react-native-datepicker';
import MapView from 'react-native-maps';
import { Actions, ActionConst } from "react-native-router-flux";
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'

// create a component
function mapStateToProps(state) {
    return {
        pickUpAddress: state.rider.tripRequest.pickUpAddress,
        destAddress: state.rider.tripRequest.destAddress,
        rider: state.rider.user,
        jwtAccessToken: state.rider.appState.jwtAccessToken,
    };
}
class ScheduleForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minDate: new Date(),
            scheduleDate: '2018-04-23',
            scheduleTime: '18:00',
            datetime: null,
            source: null,
            destination: null,
            person: 1,
            modalVisible: false,
        };
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    onSelect(index, value) {
        this.setState({
            carType: `You had selected: ${value}`
           })
    }
    createScheduleTrip(jwtAccessToken) {
        const srcAdderess = this.props.pickUpAddress;
        const destAddress = this.props.destAddress;
        const rider = this.props.rider;
        const persons = this.state.person;
        const dateTime = this.state.datetime;
        if (!srcAdderess || !destAddress || !rider || !dateTime || !persons || !jwtAccessToken) {
            alert("all fields are required")
        } else {
            const tripObject = {
                scheduleSrcAdderess: srcAdderess,
                scheduleDestAddress: destAddress,
                scheduleDateTime: dateTime,
                numberOfPerson: persons,
                tripStatus: "scheduleTripRequested",
                riderDetails: {
                    riderID: rider._id,
                    email: rider.email,
                    fname: rider.fname,
                    lname: rider.lname,
                    phoneNo: rider.phoneNo
                }
            }
            fetch(`${config.serverSideUrl}:${config.port}/api/trips/scheduleTripRequest`,
                {
                    method: "POST",
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: jwtAccessToken
                    },
                    body: JSON.stringify(tripObject)
                }).then((response) => response.json())
                .then((result) => {
                    if (result.success == true) {
                        alert('Your trip booked Successfully. Please wait for confirmation')
                        this.props.changePageStatus("home");
                        Actions.rootView();

                    } else {
                        alert("Due to some internal error, your trip can't booked riht now, Please try again later")
                    }
                })
        }


    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor="#FFB600"
                    iosBarStyle="light-content"
                    style={{ width: '100%', backgroundColor: '#FFB600' }}>
                    <Left >
                        <Ionicon name="md-arrow-back"
                            onPress={() => Actions.pop()}
                            style={styles.backIcon} />
                    </Left>
                    <Body>
                        <Text style={styles.heading}>Schedule Trip</Text>
                    </Body >
                    <Right />
                </Header>
                <Content style={{ flex: 9, backgroundColor: '#fff', width: '100%' }}>
                    <View style={{
                        width: '100%',
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <FIcon
                                name="dot-circle-o"
                                style={{ color: 'green', fontSize: 20, marginRight: 10 }} />
                            <Button
                                onPress={() => {
                                    Actions.suggestLocation({
                                        heading: "Starting Location",
                                        page: "home"
                                    });
                                }}
                                transparent
                                style={styles.textInput} >
                                <Text
                                    style={{ fontSize: 16 }}
                                    multiline={Platform.OS !== "ios"} numberOfLines={1}>
                                    {_.get(this.props, "pickUpAddress", "Source Required")}
                                </Text>
                            </Button>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <FIcon
                                name="dot-circle-o"
                                style={{ color: 'red', fontSize: 20, marginRight: 10 }} />
                            <Button
                                onPress={() => {
                                    Actions.suggestLocation({
                                        heading: "Destination Location",
                                        page: "destination"
                                    });
                                }}
                                transparent
                                style={styles.textInput}>
                                {this.props.destAddress !== "" ? (
                                    <Text
                                        style={{ fontSize: 16 }}
                                        multiline={Platform.OS !== "ios"} numberOfLines={1}>
                                        {_.get(
                                            this.props,
                                            "destAddress",
                                            "Enter Drop Location"
                                        )}
                                    </Text>
                                ) : (
                                        <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                                            Enter Drop Location
                                        </Text>
                                    )}
                            </Button>
                        </View>
                        <Text style={{ textAlign: "left", width: '100%', paddingLeft: 20, marginTop: 20, marginBottom: 10, fontWeight: 'bold' }}>Select Date and Time</Text>
                        <DatePicker
                            style={{
                                width: "90%",
                                marginBottom: 20,
                            }}
                            date={this.state.datetime}
                            mode="datetime"
                            minDate={this.state.minDate}
                            format="YYYY-MM-DD HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={(datetime) => { this.setState({ datetime: datetime }); }}
                        />

                    </View>
                    {Platform.OS === 'ios' ?
                        <View >
                            <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>No. of person : <Text>{this.state.person}</Text></Text>
                            {this.state.person <= 3 ? <Text style={{ textAlign: 'center', fontWeight: '400' }}>
                                <Image
                                    source={require('../../../../assets/images/Scab.png')}
                                    style={{ width: 60, height: 60, resizeMode: 'contain' }} />
                            </Text> :
                                <Text style={{ textAlign: 'center', fontWeight: '400' }}>
                                    <Image
                                        source={require('../../../../assets/images/van.png')}
                                        style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                                </Text>}
                            <Picker
                                selectedValue={this.state.person}
                                onValueChange={(itemValue, itemIndex) => this.setState({ person: itemValue })}>
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                            </Picker>
                        </View>
                        :
                        <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingLeft: 20, flex: 2 }}>Choose No. of person</Text>
                            <Picker
                                style={{ flex: 1 }}
                                selectedValue={this.state.person}
                                onValueChange={(itemValue, itemIndex) => this.setState({ person: itemValue })}>
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                            </Picker>
                        </View>
                        <View style={{marginTop:20}}>
                        {this.state.person <= 3 ? <Text style={{ textAlign: 'center', fontWeight: '400' }}>
                        <Image
                            source={require('../../../../assets/images/Scab.png')}
                            style={{ width: 200, height: 200, resizeMode: 'contain' }} />
                    </Text> :
                        <Text style={{ textAlign: 'center', fontWeight: '400' }}>
                            <Image
                                source={require('../../../../assets/images/van.png')}
                                style={{ width: 200, height: 200, resizeMode: 'contain' }} />
                        </Text>}
                        </View>
                        </View>
                    }

                    <Button style={{
                        alignSelf: 'center',
                        marginTop: 30,
                        backgroundColor: '#FFB600'
                    }}
                        onPress={() => this.createScheduleTrip(this.props.jwtAccessToken)}>
                        <Text style={{ color: "white" }}>Schedule My Ride</Text>
                    </Button>
                </Content>
            </View>
        );
    }
}

function bindActions(dispatch) {
    return {
        changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    };
}
//make this component available to the app
export default connect(mapStateToProps, bindActions)(ScheduleForm);
