import React, { Component } from 'react'
import {
  View, Platform, Dimensions,
  TouchableOpacity, Image, SafeAreaView,
  BackHandler, StyleSheet
} from 'react-native'
import {
  Container, Header, Content,
  Text, Button, Icon, Thumbnail,
  Card, CardItem, Title, Left,
  Right, Body, List, ListItem,
  Form, Item, Input, Label, Toast
} from "native-base";
import styles from "./styles";
import { changePageStatus } from "../../../actions/rider/home";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import config from "../../../../config";

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.rider.appState.jwtAccessToken,
    email: state.rider.user.email,
    _id: state.rider.user._id,
  };
}
export class ContactUs extends Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }

  backAndroid() {
    this.props.changePageStatus("home");
    Actions.rootView();
  }
  constructor(props) {
    super(props)

    this.state = {
      _id: this.props._id,
      subject: null,
      message: null,
      showToast: false
    }
  }

  sendMessage() {
    fetch(`${config.serverSideUrl}:${config.port}/api/query/newQuery`,
      {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.jwtAccessToken,
        },
        body: JSON.stringify({ _id: this.state._id, subject: this.state.subject, message: this.state.message })
      })
      .then(resp => resp.json()).then((convertedData) => {
        if (convertedData.success === true) {
          Toast.show({
            text: " you'll be contacted soon",
            duration: 10000,
            buttonText: 'Okay'
          }),
            Actions.pop()
        }
      })
  }

  onSend() {
    if (this.state.subject === null || this.state.message === null) {
      alert("All fields are mandatory")
    }
    else {
    this.sendMessage()
    }
  }
  render() {
    return (
      <View>
        <SafeAreaView style={{ backgroundColor: "#FFB600" }}>
          <Header
            iosBarStyle="light-content"
            androidStatusBarColor="#FFB600"
            style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
          >
            <Left>
              <Button transparent onPress={() => Actions.pop()}>
                <Icon
                  name="md-arrow-back"
                  style={{ fontSize: 28, color: "#fff" }}
                />
              </Button>
            </Left>
            <Body>
              <Title
                style={
                  Platform.OS === "ios"
                    ? styles.iosHeaderTitle
                    : styles.aHeaderTitle
                }
              >
                Contact Us
            </Title>
            </Body>
            <Right />
          </Header>
        </SafeAreaView>
        <View style={{ height: Dimensions.get("screen").height, backgroundColor: 'white', padding: 10 }}>
          <Content>
            <Form>
              <Item floatingLabel style={{ width: "80%" }}>
                <Label>Email</Label>
                <Input
                  style={{ paddingLeft: 10 }}
                  value={this.props.email}
                  editable={false} />
              </Item>
              <Item floatingLabel style={{ width: "80%" }}>
                <Label>Subject</Label>
                <Input
                  style={{ paddingLeft: 10 }}
                  onChangeText={(Subject) => this.setState({ subject: Subject })}
                />
              </Item>
            </Form>
            <View style={{
              padding: 10,
              marginTop: 20,
              height: 200,
            }}>
              <Item regular style={{ width: "100%", height: "100%", borderRadius: 10 }}>
                <Input
                  multiline={true}
                  style={{ width: "90%", height: "90%", paddingLeft: 10, paddingRight: 10 }}
                  placeholder='Write your message'
                  onChangeText={(Message) => this.setState({ message: Message })} />
              </Item>
            </View>
            <Button
              onPress={() => this.onSend()}
              style={{ width: 200, alignSelf: "center", backgroundColor: "#FFB600",  marginTop: 20 }}>
              <Text style={{ textAlign: "center", width: "100%" }}>SEND</Text>
            </Button>
            <Button
              onPress={() => Actions.myQuery()}
              style={{ width: 200, alignSelf: "center", marginBottom:10,  backgroundColor: "transparent", marginTop: 20 }}>
              <Text style={styless.queryStatus}>Show Query status</Text>
            </Button>
          </Content>
        </View>
      </View>
    )
  }
}
const styless = StyleSheet.create({
  queryStatus: {
    textAlign: "center",
    width: "100%",
    color: "#000"
  }
})
function bindActions(dispatch) {
  return {
    changePageStatus: newPage => dispatch(changePageStatus(newPage)),
  };
}

export default connect(mapStateToProps, bindActions)(ContactUs);