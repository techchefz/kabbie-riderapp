import { Platform, Dimensions } from "react-native";

export default {
    heading: {
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize: 20,
    },
    subHeading: {
        color: '#000',
        backgroundColor: 'transparent',
        fontSize: 16,
        textAlign:'center'
    },
    balance: {
        color: '#000',
        backgroundColor: 'transparent',
        fontSize: 16,
        fontSize:40,
        textAlign:'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    backView1: {
        height: Dimensions.get('screen').height,
        width: '100%',
        backgroundColor:'lightblue'
    },
    backIcon: {
        backgroundColor:'transparent',
        fontSize:30,
        color:'#fff'
    },
};
