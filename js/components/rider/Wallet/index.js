import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, StatusBar, TouchableOpacity } from 'react-native';
import { Header, Left, Right, Body, Content } from "native-base";
import { LinearGradient } from 'expo';
import Ionicon from "react-native-vector-icons/Ionicons";
import FIcon from "react-native-vector-icons/FontAwesome";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';
import config from '../../../../config';
function mapStateToProps(state) {
    return {
        email: state.rider.user.email,
        jwtAccessToken: state.rider.appState.jwtAccessToken
    }
}
class Wallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            walletBalance: 0,
        }
    }

    componentWillMount() {
        this.getWalletAmt(this.props.email, this.props.jwtAccessToken);
    }
    getWalletAmt(email, jwtAccessToken) {
        fetch(`${config.serverSideUrl}:${config.port}/api/users/claimRewards`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: jwtAccessToken,
                email: email
            }
        })
            .then(resp => resp.json())
            .then(res => {
                this.setState({ walletBalance: res.data })
            })
    }
    render() {
        this.getWalletAmt(this.props.email, this.props.jwtAccessToken);
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor="#FFB600"
                    iosBarStyle="light-content"
                    style={{ width: '100%', backgroundColor: '#FFB600' }}>
                    <Left >
                        <Ionicon name="md-arrow-back"
                            onPress={() => Actions.pop()}
                            style={styles.backIcon} />
                    </Left>
                    <Body>
                        <Text style={styles.heading}>WALLET</Text>
                    </Body >
                    <Right />
                </Header>
                <Content style={{ flex: 9, backgroundColor: '#fff', width: '100%' }}>
                    <View style={{
                        width: '100%',
                        height: Dimensions.get('window').width * (1 / 2),
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={styles.subHeading}>Your Balance</Text>
                        <Text style={styles.balance}>$ {this.state.walletBalance}</Text>
                    </View>
                </Content>
            </View>
        );
    }
}


export default connect(mapStateToProps)(Wallet);
