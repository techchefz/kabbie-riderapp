import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Header, Left, Right, Body, Content, Card, CardItem } from "native-base";
import Ionicon from "react-native-vector-icons/Ionicons";
import FIcon from "react-native-vector-icons/FontAwesome";
import { Actions } from 'react-native-router-flux';


class PaymentMethod extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor="#FFB600"
                    iosBarStyle="light-content"
                    style={{ width: '100%', backgroundColor: '#FFB600' }}>
                    <Left style={{ flex: 1 }}>
                        <Ionicon name="md-arrow-back"
                            onPress={() => Actions.pop()}
                            style={styles.backIcon} />
                    </Left>
                    <Body style={{ flex: 8, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.heading}>Choose payment option</Text>
                    </Body >
                    <Right style={{ flex: 1 }} />
                </Header>
                <Content style={{ flex: 1, width: '100%', paddingTop:40 }}>
                    <TouchableOpacity 
                        onPress={()=> Actions.creditCard()}
                        style={styles.card}>
                        <FIcon
                            name="credit-card-alt"
                            style={{ fontSize: 18, flex:1 }} />
                        <Text style={{flex:8}}>Credit/Debit Card</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={()=> alert('Coming Soon')}
                        style={[styles.card, {backgroundColor:"#e4e4e4"}]}>
                        <FIcon
                            name="paypal"
                            style={{ fontSize: 18, flex:1 }} />
                        <Text style={{flex:8}}>Paypal</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems:'center',
        backgroundColor: '#fff',
    },
    heading: {
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize: 20,
    },
    backIcon: {
        backgroundColor: 'transparent',
        fontSize: 30,
        color: '#fff'
    },
    card: {
        backgroundColor:'#e8f4fd',
        paddingLeft:10,
        justifyContent: 'flex-start',
        alignItems:'center',
        marginLeft:10,
        borderWidth:0.5,
        borderColor:'gray',
        borderRadius:5,
        width:'90%',
        height:40,
        flexDirection:'row',
        marginBottom:10
    }
});

export default PaymentMethod;
