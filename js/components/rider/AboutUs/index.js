import React, { Component } from 'react'
import { View, Platform, Dimensions, TouchableOpacity, Image, SafeAreaView, BackHandler } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import { changePageStatus } from "../../../actions/rider/home";
import { connect } from "react-redux";


export class AboutUs extends Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }

  backAndroid() {
    this.props.changePageStatus("home");
    Actions.rootView();
  }
  render() {
    return (
      <View>
        <SafeAreaView style={{ backgroundColor: "#FFB600" }}>
          <Header
            iosBarStyle="light-content"
            androidStatusBarColor="#FFB600"
            style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
          >
            <Left>
              <Button transparent onPress={() => Actions.pop()}>
                <Icon
                  name="md-arrow-back"
                  style={{ fontSize: 28, color: "#fff" }}
                />
              </Button>
            </Left>
            <Body>
              <Title
                style={
                  Platform.OS === "ios"
                    ? styles.iosHeaderTitle
                    : styles.aHeaderTitle
                }
              >
                About Us
            </Title>
            </Body>
            <Right />
          </Header>
        </SafeAreaView>
        <View style={{ height: Dimensions.get("screen").height, backgroundColor: 'white' }}>
          <View style={{ flex: 1 }}>
            <View style={{ alignItems: 'center', justifyView: 'center', marginTop:30 }}>
              <Image
                source={require("../../../../assets/images/kabbie_logo.png")}
                style={{
                  width: '80%',
                  height: Dimensions.get("screen").height * (1 / 3),
                  resizeMode: "contain"
                }} />
            </View>
            <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', fontWeight: "100" }}>Version 1.0.1</Text>
            <Text
              onPress={() => alert('Terms & conditions')}
              style={{
                textAlign: 'center',
                marginTop:30
              }}>Terms {"&"} Conditions</Text>

          </View>
        </View>
      </View>
    )
  }
}
function mapStateToProps(state) {
  return {
      rider: state.rider.user,
  };
}
function bindActions(dispatch) {
  return {
      changePageStatus: newPage => dispatch(changePageStatus(newPage)),
  };
}
export default connect(mapStateToProps, bindActions)(AboutUs);