import commonColor from '../../../../native-base-theme/variables/commonColor';

export default {
  iosHeader: {
    backgroundColor: '#FFB600',

  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#383838",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#383838",
  },
  orText: {
    textAlign: 'center',
    fontWeight: '700',
    color: '#fff',
  },
  regBtnContain: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  regBtn: {
    height: 50,
    borderRadius: 5,
    backgroundColor: "#FFB600",
  },
  googleLeft: {
    flex: 1,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#B6382C",
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4
  },
  fbLeft: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#233772",
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4
  }
};
