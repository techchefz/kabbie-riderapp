import * as Expo from "expo";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Platform, View, TouchableOpacity, KeyboardAvoidingView, Dimensions, SafeAreaView, PixelRatio } from "react-native";
import PropTypes from "prop-types";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Body,
  Spinner,
  Toast,
  Grid,
  Col,
  Thumbnail
} from "native-base";
import { Actions } from "react-native-router-flux";
import RegisterFormFb from "../register/formFb";
import * as userSelector from "../../../reducers/rider/user";
import * as appStateSelector from "../../../reducers/rider/appState";
import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import { changePageStatus } from "../../../actions/rider/home";
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";
import { requestFbLogin } from "../loginFb";
import { signInWithGoogleAsync } from "../loginGoogle";
import { checkUser, userLoginRequest } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

function mapStateToProps(state) {
  return {
    loadingStatus: state.rider.appState.loadingStatus,
    isLoggedIn: state.rider.appState.isLoggedIn,
    loginError: state.rider.appState.loginError,
    userType: userSelector.getUserType(state),
    errormsg: appStateSelector.getErrormsg(state),
    isFetching: appStateSelector.isFetching(state),
    socialLogin: state.entrypage.socialLogin,
    pageStatus: state.rider.appState.pageStatus,
    appConfig: state.basicAppConfig.config
  };
}
class SignIn extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    signinAsync: PropTypes.func,
    socailLoginSuccessAndRoutetoRegister: PropTypes.func,
    socailSignupSuccess: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      socialLogin: null
    };
  }
  state = {
    showError: false
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.loginError) {
      this.setState({
        showError: true
      });
    } else {
      this.setState({
        showError: false
      });
    }
    if (nextProps.socialLogin.email !== null) {
      this.setState({ socialLogin: nextProps.socialLogin });
    }
  }

  showLoaderModal() {
    return (
      <ModalView>
        <Spinner />
      </ModalView>
    );
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <SafeAreaView style={{backgroundColor:"#FFB600"}}/>
        <Header
          androidStatusBarColor="#FFB600"
          iosBarStyle="dark-content"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{
                  fontSize: 28,
                  marginLeft: 5,
                  color: "#383838"
                }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >
              Sign In
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={{ padding: 10 }} scrollEnabled bounces={false}>
        <KeyboardAvoidingView style={{ padding: 10 }} scrollEnabled bounces={false}>

{/* -------------------------------------ICON-------------------------------------- */}
<View style={{ height: Dimensions.get("window").height * (2 / 10), justifyContent: 'center', alignItems: 'center' }}>
  <Thumbnail
    source={require('../../../../assets/images/kabbie_logo.png')}
    style={{ width: "100%", height: PixelRatio.getPixelSizeForLayoutSize(50), resizeMode: 'contain' }} />
</View>

{/* -------------------------------------LOGIN TEXTINPUT-------------------------------------- */}
          <View style={{ padding: 10 }}>
            {this.state.socialLogin && (
              <RegisterFormFb socialLogin={this.state.socialLogin} />
            )}
            {!this.state.socialLogin && (
              <LoginForm isFetching={this.props.isFetching} />
            )}
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 20 }}>
              <View style={{ borderBottomWidth: 0.5, width: "40%" }} />
              <View style={{ width: "30%", }}>
                <Text style={{ textAlign: 'center' }}>OR</Text>
              </View>
              <View style={{ borderBottomWidth: 0.5, width: "40%" }} />
            </View>
              <TouchableOpacity
            style={{
              flexDirection: 'row',
              backgroundColor: "#3b5998",
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
              height:50, 
              borderRadius:5,
              marginBottom:30,
            }}
            onPress={() =>
              requestFbLogin(
                this.props.socailSignupSuccess,
                this.props.appConfig.facebookAuth,
                this.props.checkUser,
                this.props.userLoginRequest
              )
            }>
            <View style={{marginRight:20}}><FAIcon name="facebook" size={20} color="#fff"/></View>
            <View><Text style={{ color: "white" }}>Sign up with Facebook</Text></View>
            </TouchableOpacity>
            {/* {this.state.showError &&
              Toast.show({
                text: this.props.errormsg,
                position: "bottom",
                duration: 1500
              })} */}
          </View>
          {this.props.loadingStatus ? this.showLoaderModal() : null}
          </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    checkUser: (obj1, obj2) => dispatch(checkUser(obj1, obj2)),
    userLoginRequest: () => dispatch(userLoginRequest()),
    clearEntryPage: () => dispatch(clearEntryPage()),
    socailLoginSuccessAndRoutetoRegister: data =>
      dispatch(socailLoginSuccessAndRoutetoRegister(data)),
    socailSignupSuccess: route => dispatch(socailSignupSuccess(route)),
    signinAsync: userCredentials => dispatch(signinAsync(userCredentials)),
    changePageStatus: pageStatus => dispatch(changePageStatus(pageStatus))
  };
}

export default connect(mapStateToProps, bindActions)(SignIn);
