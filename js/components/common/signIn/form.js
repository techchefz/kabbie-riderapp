import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Text, Dimensions, Modal, TouchableOpacity, Platform, TextInput } from 'react-native';
import { Item, Input, Button, View, Icon, Content, Toast } from 'native-base';
import PropTypes from 'prop-types';
import { signinAsync, forgotMail } from '../../../actions/common/signin';
import Spinner from '../../loaders/Spinner';
import CountryPicker from "react-native-country-picker-modal";
import config from "../../../../config";
import FAIcon from "react-native-vector-icons/FontAwesome";
import styles from './styles';
import commonColor from '../../../../native-base-theme/variables/commonColor';

const deviceWidth = Dimensions.get('window').width;
const validate = values => {
  const errors = {};
  if (!values.phoneNo) {
    errors.phoneNo = "Phone number is Required";
    // } else if (!values.phoneNo.match(/^\d{10}$/)) {
    //   errors.phoneNo = "Invalid phone number";
    // } else if (isNaN(Number(values.phoneNo))) {
    //   errors.phoneNo = 'Must be a number';
  } else if (!values.password) {
    errors.password = 'Password is Required';
  } else if (!values.phoneNo) {
    errors.phoneNo = 'Mobile Number is Required';
  } else if (!values.fname) {
    errors.fname = 'First name is Required';
  } else if (!values.lname) {
    errors.lname = 'Last name is Required';
  }
  if (!values.password) {
    errors.password = 'Password is Required';
  }
  return errors;
};

export const input = props => {
  const { meta, input } = props;
  return (
    <View>
      <Item>
        {props.type === 'phoneNo' ? (
          null
        ) : (
            props.type === 'email' ? (
              <Icon
                name="ios-call-outline"
                style={{ color: commonColor.lightThemePlaceholder }}
              />
            ) : (
                <Icon
                  name="ios-lock-outline"
                  style={{ color: commonColor.lightThemePlaceholder }}
                />
              )
          )}
        <Input {...input} {...props} />
      </Item>

      {meta.touched &&
        meta.error && <Text style={{ color: 'red' }}>{meta.error}</Text>}
    </View>
  );
};
input.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object
};

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      cca2: "IN",
      callingCode: "91",
      name: "IN",
      modalVisible: false,
      email: null,
      mobileNo: null,
      emailError: false,
      emailNotFound: false,
      confirmEmail: true,
      confirmOTP: false,
      emptyOTP: false,
      errorOTPmatch: false,
      resetPassword: false,
      enterOTP: null,
      phoneNo: null,
      userEmail: null,
      enterNewPassword: null,
      enterConfirmPassword: null,
      otpNotFound: false,
      passwordUnmatched: false
    };
  }
  static propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func,
    forgotMail: PropTypes.func,
    isFetching: PropTypes.bool
  };
  submit(values) {
    // values.phoneNo = this.state.callingCode + values.phoneNo;
    this.props.dispatch(signinAsync(values, this.state.callingCode));
  }
  forgotPassword() {
    this.props.dispatch(forgotMail());
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  onConfirmEmail() {
    this.state.phoneNo === null ? this.setState({ emailError: true }) :
      this.enterOtp()
  }
  enterOtp() {
    this.setState({ emailError: false })
    return fetch(`${config.serverSideUrl}:${config.port}/api/config/forgot`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        phoneNo: '+' + this.state.callingCode + this.state.phoneNo
      })
    }).then((response) => response.json())
      .then((result) => {
        console.log('================result====================')
        console.log(result)
        console.log('====================================')
        if (result.success == false && result.message == "Otp is Required") {
          this.setState({
            confirmEmail: false,
            confirmOTP: true,
            resetPassword: false
          })
          //send to new page with phone No.
        } else {
          this.setState({
            emailNotFound: true
          })
          //show err message
        }
      }).catch((error) => {
        console.error(error);
      });
  }
  matchOtp() {
    if (this.state.enterOTP === null) {
      this.setState({
        emptyOTP: true
      })
    } else {
      this.setState({
        emptyOTP: false
      })
      return fetch(`${config.serverSideUrl}:${config.port}/api/config/forgot`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ phoneNo: '+' + this.state.callingCode + this.state.phoneNo, otp: this.state.enterOTP, newPassword: "" })
      }).then((response) => response.json())
        .then((result) => {
          const phoneNo = result.data.phoneNo;
          const email = result.data.email;
          const success = result.success;
          if (success) {
            this.setState({
              confirmEmail: false,
              confirmOTP: false,
              resetPassword: true
            })
            //send to new page with phone No.
          } else {
            this.setState({
              otpNotFound: true
            })
            //show err message
          }
        }).catch((error) => {
          console.error(error);
        });
    }
  }
  resetPassword() {
    if (this.state.enterNewPassword === this.state.enterConfirmPassword) {
      return fetch(`${config.serverSideUrl}:${config.port}/api/config/forgot`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ phoneNo: '+' + this.state.callingCode + this.state.phoneNo, otp: this.state.enterOTP, newPassword: this.state.enterNewPassword })
      }).then((response) => response.json())
        .then((result) => {
          const phoneNo = result.data.phoneNo;
          const email = result.data.email;
          const success = result.success;
          if (success) {
            [
              this.setState({
                confirmEmail: false,
                confirmOTP: false,
                resetPassword: true
              }),
              this.setModalVisible(!this.state.modalVisible),
              Toast.show({
                duration: 5000,
                text: "password reset successfully",
                buttonText: "Okay",
                type: "success"
              })
              //  alert('password reset successfully')
              //send to new page with phone No.
            ]
          } else {
            console.log('====================================')
            console.log("reset error")
            console.log('====================================')
            //show err message
          }
        }).catch((error) => {
          console.error(error);
        });
    } else {
      this.setState({
        passwordUnmatched: true
      })
    }
  }
  render() {
    return (
      <View>
        {/* =============================RESET PASSWORD======================= */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{
            flex: 1,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            width: Dimensions.get("screen").width,
            justifyContent: "center",
            alignItems: "center"
          }}>
            <Content
              contentContainerStyle={{
                flex: 1,
                backgroundColor: 'transparent',
                width: Dimensions.get("screen").width,
                maxWidth: 350,
                justifyContent: 'center',
                alignItems: 'center'
              }}>
              <View style={{
                justifyContent: 'space-around',
                backgroundColor: '#fff',
                width: "90%",
                maxWidth: 350,
                height: 300,
                borderRadius: 10,
                ...Platform.select({
                  ios: {
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 4,
                      height: 4
                    },
                    shadowRadius: 5,
                    shadowOpacity: 0.5
                  },
                  android: { elevation: 4 }
                })
              }}>
                {this.state.confirmEmail === true ? (
                  <View style={{ width: '100%', height: '100%' }}>
                    <View style={{
                      flex: 2,
                      backgroundColor: '#FFB600',
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                      <Text style={{ backgroundColor: 'transparent', color: 'white', fontSize: 18 }}>Confirm Mobile Number</Text>
                    </View>
                    <View style={{ flex: 8, padding: 10, height: '100%', justifyContent: 'space-around' }}>
                      <View style={{ justifyContent: "center", alignItems: "center" }}>
                        <View style={{ padding: 10, flexDirection: "row" }}>
                          <View style={{
                            width: "10%", justifyContent: "center",
                            alignItems: "center", borderBottomWidth: 0.5, borderBottomColor: commonColor.brandPrimary
                          }}>
                            <CountryPicker
                              cca2={this.state.cca2}
                              onChange={value =>
                                this.setState({
                                  cca2: value.cca2,
                                  callingCode: value.callingCode
                                })
                              }
                            />
                          </View>
                          <View style={{ width: "90%" }}>
                            <TextInput
                              maxLength={10}
                              minLength={10}
                              underlineColorAndroid="transparent"
                              placeholder="Mobile Number"
                              placeholderTextColor="#383838"
                              onChangeText={(phoneNo) => this.setState({ phoneNo })}
                              value={this.state.phoneNo}
                              keyboardType="phone-pad"
                              autoCapitalize="none"
                              style={{ height: 40, borderColor: 'gray', borderBottomWidth: 0.5, marginLeft: 20, width: "80%" }}
                            />
                          </View>
                        </View>

                        {this.state.emailError === true ?
                          <Text style={{ color: 'red' }}>Enter Mobile Number</Text> : null
                        }
                        {this.state.emailNotFound === true ? (
                          <Text
                            style={{ textAlign: 'center', color: 'red' }}>Invalid User</Text>
                        ) : null}
                      </View>
                      <View style={{ alignSelf: 'center', flexDirection: "row" }}>
                        <Button
                          style={{ backgroundColor: 'green' }}
                          title="Confirm Email"
                          onPress={() => {
                            this.onConfirmEmail()
                          }}><Text style={{ color: 'white' }}>Confirm Number</Text></Button>
                        <TouchableOpacity
                          onPress={() => this.setModalVisible(!this.state.modalVisible)}
                          style={{ justifyContent: "center", alignItems: "center", marginLeft: 20 }}>
                          <FAIcon
                            name="window-close-o"
                            style={{
                              fontSize: 40,
                              color: "#ff3333"
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : (
                    this.state.confirmOTP === true ? (
                      <View style={{ width: '100%', height: '100%', }}>
                        <View style={{
                          flex: 2,
                          backgroundColor: '#FFB600',
                          borderTopLeftRadius: 10,
                          borderTopRightRadius: 10,
                          justifyContent: 'center',
                          alignItems: 'center'
                        }}>
                          <Text style={{ backgroundColor: 'transparent', color: 'white', fontSize: 18 }}>Verify yourself</Text>
                        </View>
                        <View style={{ flex: 8, padding: 10, height: '100%', justifyContent: 'space-around' }}>
                          <View>
                            <TextInput
                              style={{
                                alignSelf: "center",
                                height: 40,
                                width: "70%",
                                marginBottom: 20,
                                borderRadius: 20,
                                marginTop: 20,
                                borderColor: 'gray',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderWidth: 1
                              }}
                              underlineColorAndroid="transparent"
                              keyboardType="numeric"
                              textAlign={'center'}
                              placeholder="Enter Your 6 Digit Otp"
                              maxLength={6}
                              onChangeText={(enterOTP) => this.setState({ enterOTP })}
                              value={this.state.enterOTP}
                            />
                            {/* <TextInput
                            placeholder="Enter OTP"
                            keyboardType="numeric"
                            underlineColorAndroid="false"
                            maxLength={6}
                            style={{ height: 40, borderColor: 'gray', borderBottomWidth: 0.5 }}
                            onChangeText={(enterOTP) => this.setState({ enterOTP })}
                            value={this.state.enterOTP}
                          /> */}
                            {this.state.emptyOTP === true ?
                              <Text style={{ color: 'red' }}>Please Enter OTP</Text> : null
                            }
                            {this.state.otpNotFound === true ? (
                              <Text
                                style={{ textAlign: 'center', color: 'red' }}>Please enter valid OTP</Text>
                            ) : null}
                          </View>
                          <View style={{ alignSelf: 'center', flexDirection: "row" }}>
                            <Button
                              style={{ backgroundColor: 'green' }}
                              onPress={() => {
                                this.matchOtp()
                              }}><Text style={{ color: 'white' }}>Verify Me</Text></Button>
                            <TouchableOpacity
                              onPress={() => this.setModalVisible(!this.state.modalVisible)}
                              style={{ justifyContent: "center", alignItems: "center", marginLeft: 20 }}>
                              <FAIcon
                                name="window-close-o"
                                style={{
                                  fontSize: 40,
                                  color: "#ff3333"
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    ) : (
                        <View style={{ width: '100%', height: '100%' }}>
                          <View style={{
                            flex: 2,
                            backgroundColor: '#FFB600',
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10,
                            justifyContent: 'center',
                            alignItems: 'center'
                          }}>
                            <Text style={{ backgroundColor: 'transparent', color: 'white', fontSize: 18 }}>Verify yourself</Text>
                          </View>
                          <View style={{ flex: 8, padding: 10, height: '100%', justifyContent: 'space-around' }}>
                            <View>
                              <TextInput
                                underlineColorAndroid="transparent"
                                placeholder="Enter New Password"
                                autoCapitalize={false}
                                secureTextEntry={true}
                                style={{ height: 40, borderColor: 'gray', borderBottomWidth: 0.5 }}
                                onChangeText={(enterNewPassword) => this.setState({ enterNewPassword })}
                                value={this.state.enterNewPassword}
                              />
                              <TextInput
                                underlineColorAndroid="transparent"
                                placeholder="Confirm Password"
                                autoCapitalize={false}
                                secureTextEntry={true}
                                style={{ height: 40, borderColor: 'gray', borderBottomWidth: 0.5 }}
                                onChangeText={(enterConfirmPassword) => this.setState({ enterConfirmPassword })}
                                value={this.state.enterConfirmPassword}
                              />
                              {this.state.passwordUnmatched == true ?
                                <Text style={{ color: 'red' }}>Password didn't matched</Text> : null
                              }
                            </View>
                            <View style={{ alignSelf: 'center' }}>
                              <Button
                                style={{ backgroundColor: 'green' }}
                                onPress={() => {
                                  this.resetPassword()
                                }}><Text style={{ color: 'white' }}>Reset Password</Text></Button>
                            </View>
                            <Text
                              onPress={() => this.setModalVisible(!this.state.modalVisible)}
                              style={{ textAlign: 'center', marginTop: 10 }}>Cancel</Text>
                          </View>
                        </View>
                      )
                  )}
              </View>
            </Content>
          </View>
        </Modal>
        {/* =============================RESET PASSWORD======================= */}
        <View style={{ padding: 10, flexDirection: "row" }}>
          <View style={{
            width: "10%", justifyContent: "center",
            alignItems: "center", borderBottomWidth: 0.5, borderBottomColor: commonColor.brandPrimary
          }}>
            <CountryPicker
              cca2={this.state.cca2}
              onChange={value =>
                this.setState({
                  cca2: value.cca2,
                  callingCode: value.callingCode
                })
              }
            />
          </View>
          <View style={{ width: "90%" }}>
            <Field
              component={input}
              type="phoneNo"
              name="phoneNo"
              maxLength={10}
              minLength={10}
              placeholder="Mobile Number"
              placeholderTextColor="#383838"
              keyboardType="numeric"
              autoCapitalize="none"
            />
          </View>
        </View>
        <View style={{ padding: 10 }}>
          <Field
            component={input}
            placeholder="Password"
            secureTextEntry
            placeholderTextColor="#383838"
            name="password"
            autoCapitalize="none"
          />
        </View>
        <View style={styles.regBtnContain}>
          <Button
            onPress={this.props.handleSubmit(this.submit.bind(this))}
            block
            style={styles.regBtn}
          >
            {this.props.isFetching ? (
              <Spinner />
            ) : (
                <Text style={{ color: '#383838', fontWeight: 'bold', fontSize: 16 }}>Sign In</Text>
              )}
          </Button>
        </View>
        <Button
          transparent
          style={{ left: deviceWidth - 200 }}
          onPress={() => {
            [
              this.setModalVisible(true),
              this.setState({
                email: null,
                emailError: false,
                emailNotFound: false,
                confirmEmail: true,
                confirmOTP: false,
                emptyOTP: false,
                errorOTPmatch: false,
                resetPassword: false,
                enterOTP: null,
                phoneNo: null,
                userEmail: null,
                enterNewPassword: null,
                enterConfirmPassword: null,
                otpNotFound: false,
                passwordUnmatched: false
              })
            ]
          }}
        // onPress={this.forgotPassword.bind(this)}
        >
          <Text style={{ color: 'red' }}>Forgot Password ?</Text>
        </Button>
      </View>
    );
  }
}
export default reduxForm({
  form: 'login', // a unique name for this form
  validate
})(LoginForm);
