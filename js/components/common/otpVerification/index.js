//import liraries
import React, { Component } from 'react';
import { Text, StyleSheet, Dimensions, TextInput, TouchableOpacity, StatusBar, SafeAreaView } from 'react-native';
import { Item, Input, Button, Grid, Col, View, Container, Content } from "native-base";
import { connect } from 'react-redux';
import config from "../../../../config";
import { Actions } from "react-native-router-flux";
import CodeInput from 'react-native-confirmation-code-input';
import RootView from '../../rider/rootView';


class OtpVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enteredOtpValue: null,
            btnTitle: 'Verify'
        };
    }
    onVerifyClick(otp, email, jwtAccessToken) {
        fetch(`${config.serverSideUrl}:${config.port}/api/verify/mobile?otp=${otp}&email=${email}`,
            {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: jwtAccessToken,
                }
            })
            .then(r => r.json())
            .then(res => {
                if (res.success == true) {
                    Actions.riderStartupService();
                } else {
                    alert("Enter a Valid OTP!")
                    this.setState({ btnTitle: "Retry" })
                }
            });
    }

    async onReSendClick(email, jwtAccessToken) {
        await fetch(`${config.serverSideUrl}:${config.port}/api/verify/mobile/reSend`, {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: jwtAccessToken,
                email: email
            }
        })
    }
    render() {
        return (
            <Container style={styles.container} >
                <StatusBar
                    barStyle='light-content' />
                    <SafeAreaView style={{backgroundColor:"#FFB600"}}>
                <View style={{ borderBottomWidth: 0.5, height: 40, backgroundColor: '#FFB600', justifyContent: 'flex-start', alignItems: 'center', width: Dimensions.get('screen').width }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>OTP VERFICATION</Text>
                </View>
                </SafeAreaView>
                <Content
                    style={{
                        flex: 1,
                        width: Dimensions.get('screen').width
                    }}>
                    <View
                        style={styles.InnerContainer}>
                        <Text
                            style={{ textAlign: 'center', width: '70%', marginBottom: 30 }}>
                            We have sent you an OTP Via SMS on {this.props.phoneNo} for Mobile number verification
                        </Text>
                        <Text
                            style={{ textAlign: 'center', width: '70%', color: '#afafaf', marginBottom: 20 }}>
                            Enter your one time password (OTP)
                        </Text>
                        <View style={{
                            width: "100%", height: 100, justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <TextInput
                                style={{
                                    height: 40,
                                    width: "70%",
                                    marginBottom: 20,
                                    borderRadius: 20,
                                    marginTop: 20,
                                    borderColor: 'gray',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderWidth: 1
                                }}
                                underlineColorAndroid="transparent"
                                keyboardType="numeric"
                                textAlign={'center'}
                                placeholder="Enter Your 6 Digit Otp"
                                maxLength={6}
                                onChangeText={(text) => this.setState({ enteredOtpValue: text })}
                            />
                        </View>

                        <TouchableOpacity
                            onPress={() => this.onVerifyClick(this.state.enteredOtpValue, this.props.email, this.props.jwtAccessToken)}
                            style={styles.verifyBtn}
                        >
                            <Text
                                style={styles.btnTitle}>{this.state.btnTitle}</Text>
                        </TouchableOpacity>
                        <Text style={{ marginTop: 20, color: 'black', backgroundColor: 'transparent' }}>Not Received Otp ?</Text>

                        <TouchableOpacity
                            style={styles.resendBtn}
                            onPress={() => this.onReSendClick(this.props.email, this, this.props.jwtAccessToken)}
                        >
                            <Text
                                style={[styles.btnTitle, { color: 'red' }]}>Resend OTP</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: Dimensions.get('window').height,
        alignItems: 'center'
    },
    InnerContainer: {
        width: '100%',
        height: "100%",
        backgroundColor: 'white',
        borderRadius: 5,
        marginTop: 100,
        alignItems: 'center'
    },
    btnTitle: {
        color: 'white',
        fontWeight: 'bold',
        backgroundColor: 'transparent'
    },
    verifyBtn: {
        height: 30,
        backgroundColor: '#FFB600',
        width: "70%",
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 20
    },
    resendBtn: {
        height: 30,
        width: "70%",
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    }
})

function mapStateToProps(state) {
    return {
        phoneNo: state.rider.user.phoneNo,
        email: state.rider.user.email,
        jwtAccessToken: state.rider.appState.jwtAccessToken,
    }
}

export default connect(mapStateToProps, null)(OtpVerification);
