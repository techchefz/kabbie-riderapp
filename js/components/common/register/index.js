import * as Expo from "expo";
import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Platform, TouchableOpacity, SafeAreaView } from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Body,
  Spinner,
  Toast,
  Grid,
  Col
} from "native-base";
import { Actions } from "react-native-router-flux";
import { requestFbLogin } from "../loginFb";
import { registerWithGoogleAsync } from "../loginGoogle";
import * as appStateSelector from "../../../reducers/rider/appState";
import { checkUser, userLoginRequest } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";

import RegisterForm from "./form";
import RegisterFormFb from "./formFb";

import { registerAsync } from "../../../actions/common/register";
import {
  clearEntryPage,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import commonColor from "../../../../native-base-theme/variables/commonColor";
import styles from "./styles";

function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.rider.appState.errormsg) {
      return "";
    } else return state.rider.appState.errormsg;
  };

  return {
    loadingStatus: state.rider.appState.loadingStatus,
    isLoggedIn: state.rider.appState.isLoggedIn,
    registerError: state.rider.appState.registerError,
    socialLogin: state.entrypage.socialLogin,
    errormsg: getErrormsg(),
    isFetching: appStateSelector.isFetching(state),
    appConfig: state.basicAppConfig.config
  };
}
class Register extends Component {
  static propTypes = {
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    socailSignupSuccess: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      checkDriver: false,
      registerError: false,
      FirstNameChange: "",
      LastNameChange: "",
      EmailChange: "",
      MobileNumberChange: "",
      PasswordChange: "",
      socialLogin: null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.registerError) {
      this.setState({ registerError: true });
    }
    if (nextProps.socialLogin.email !== null) {
      this.setState({ socialLogin: nextProps.socialLogin });
    }
  }

  showLoaderModal() {
    return (
      <ModalView>
        <Spinner />
      </ModalView>
    );
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <SafeAreaView style={{backgroundColor:"#FFB600"}}/>      
        <Header
          iosBarStyle="dark-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{
                  fontSize: 28,
                  marginLeft: 5,
                  color: "#383838"
                }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >
              Register
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={{ padding: 10 }} scrollEnabled bounces={false}>

          <View style={{ padding: 10 }}>

            {this.state.socialLogin && (
              <RegisterFormFb socialLogin={this.state.socialLogin} />
            )}
            {!this.state.socialLogin && (
              <RegisterForm isFetching={this.props.isFetching} />
            )}
            {this.state.registerError &&
              Toast.show({
                text: this.props.errormsg,
                position: "bottom",
                duration: 1500
              })}
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 20 }}>
            <View style={{ borderBottomWidth: 0.5, width: "40%" }} />
            <View style={{ width: "30%", }}>
              <Text style={{ textAlign: 'center' }}>OR</Text>
            </View>
            <View style={{ borderBottomWidth: 0.5, width: "40%" }} />
          </View>
          <View style={{
            width: '90%',
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height:50, 
            borderRadius:5
          }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              backgroundColor: "#3b5998",
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
              height:50, 
              borderRadius:5
            }}
            onPress={() =>
              requestFbLogin(
                this.props.socailSignupSuccess,
                this.props.appConfig.facebookAuth,
                this.props.checkUser,
                this.props.userLoginRequest
              )
            }>
            <View style={{marginRight:20}}><FAIcon name="facebook" size={20} color="#fff"/></View>
            <View><Text style={{ color: "white" }}>Sign up with Facebook</Text></View>
            </TouchableOpacity>
          </View>
          {/* {this.props.appConfig.enableFacebook ? (
            <Button
              onPress={() =>
                requestFbLogin(
                  this.props.socailSignupSuccess,
                  this.props.appConfig.facebookAuth,
                  this.props.checkUser,
                  this.props.userLoginRequest
                )
              }
              block
              style={{
                paddingLeft: 0,
                backgroundColor: "#3B579D",
                borderRadius: 4,
                height: 50
              }}
            >
              <Left style={styles.fbLeft}>
                <FAIcon
                  name="facebook"
                  style={{ fontSize: 30, color: "#fff" }}
                />
              </Left>
              <Body style={{ flex: 3 }}>
                <Text
                  style={{
                    color: "#fff",
                    fontWeight: "bold",
                    textAlign: "center"
                  }}
                >
                  Sign up with Facebook
                </Text>
              </Body>
              <Right />
            </Button>
          ) : (
            <View />
          )} */}
          {this.props.loadingStatus ? this.showLoaderModal() : null}
          <View style={{width:"100%", height:90}}/>
        </Content>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    checkUser: (obj1, obj2) => dispatch(checkUser(obj1, obj2)),
    userLoginRequest: () => dispatch(userLoginRequest()),
    clearEntryPage: () => dispatch(clearEntryPage()),
    socailSignupSuccess: route => dispatch(socailSignupSuccess(route)),
    registerAsync: userCredentials => dispatch(registerAsync(userCredentials)),
    registerAsyncFb: userObj => dispatch(registerAsyncFb(userObj))
  };
}

export default connect(mapStateToProps, bindActions)(Register);
