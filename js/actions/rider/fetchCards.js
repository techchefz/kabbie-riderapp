import config from '../../../config';


export const SET_CARD_DETAILS = "SET_CARD_DETAILS";

export function setCard(cardDetails) {
    return {
        type: SET_CARD_DETAILS,
        payload: cardDetails
    };
}

export function fetchCards(email, jwtAccessToken) {
    return dispatch => {
        fetch(`${config.serverSideUrl}:${config.port}/api/users/fetchCardDetails`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: jwtAccessToken,
                email: email
            }
        })
            .then(resp => resp.json())
            .then(res => {
                dispatch(setCard(res.data))
            })
    }
}