import config from '../../../config';

export const FETCH_USERS_REFERRED = "FTCH_USERS_REFERRED";

export function fetchUsersRefered(referredUsers) {
    return {
        type: FETCH_USERS_REFERRED,
        payload: referredUsers,
    };
}

export function getReferredUsers(email, jwtAccessToken) {
    return dispatch => {
        fetch(`${config.serverSideUrl}:${config.port}/api/users/fetchReferredUsers`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: jwtAccessToken,
            },
            body: JSON.stringify({ email: email })
        })
            .then(resp => resp.json())
            .then(res => {
                dispatch(fetchUsersRefered(res))
            })
    }
}