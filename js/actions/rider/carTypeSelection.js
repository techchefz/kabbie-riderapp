export const CHANGE_CAR_TYPE = "CHANGE_CAR_TYPE";

export function changeCarType(type) {
    return {
      type: CHANGE_CAR_TYPE,
      payload: type,
    };
  }