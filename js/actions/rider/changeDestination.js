export const CHANGE_DESTINATION = "CHANGE_DESTINATION";
export const CONFIRM_BTN = "CONFIRM_BTN";

export function changeDestination(type) {
  return {
    type: CHANGE_DESTINATION,
    payload: type,
  };
}



export function confirmBtn(type) {
  return {
    type: CONFIRM_BTN,
    payload: type,
  };
}