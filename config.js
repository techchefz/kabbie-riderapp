export default {
  // local server
  // serverSideUrl: "http://192.168.0.115", // use Ip address instead of localhost
  serverSideUrl: "http://188.166.68.8",// use Ip address instead of localhost
  port: 5010 // incase of running api-server (node app) in development
  // port: 3066 //incase of running api-server (node app) in production
  //  port: 443 //incase of running api-server (node app) on heroku
};
